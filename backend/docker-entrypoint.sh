#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z db 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

python manage.py makemigrations
python manage.py migrate 

python manage.py loaddata seeders/departamento.json 
python manage.py loaddata seeders/municipio.json
python manage.py loaddata seeders/user.json
python manage.py loaddata seeders/dominios.json  
python manage.py loaddata seeders/listas.json  

exec "$@"