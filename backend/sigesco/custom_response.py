from collections import OrderedDict
from rest_framework import status

class CustomResponse():

    def HTTP_201_CREATED():
        return OrderedDict([
            ('message', "Registro guardado exitosamente"),
            ('status', status.HTTP_201_CREATED),
        ])
    
    def HTTP_200_OK():
        return OrderedDict([
            ('message', "Proceso realizado correctamente"),
            ('status', status.HTTP_200_OK),
        ])
