try:
    from django.core.management.base import NoArgsCommand
except ImportError:
    from django.core.management import BaseCommand as NoArgsCommand

from jet.utils import get_app_list


class Command(NoArgsCommand):
    help = 'Genera un ejemplo de configuración de aplicaciones personalizadas JET'
    item_order = 0
    
    def handle(self, *args, **options):
        if args:
            raise CommandError("El comando no acepta ningún argumento")
        return self.handle_noargs(**options)
    
    def handle_noargs(self, **options):
        class User:
            is_active = True
            is_staff = True
            is_superuser = True

            def has_module_perms(self, app):
                return True

            def has_perm(self, object):
                return True

        class Request:
            user = User()

        app_list = get_app_list({
            'request': Request(),
            'user': None
        })

        self.stdout.write('# Agregue esto a su settings.py para personalizar la lista de aplicaciones y modelos')
        self.stdout.write('JET_SIDE_MENU_CUSTOM_APPS = [')

        for app in app_list:
            app_label = app.get('app_label', app.get('name'))

            self.stdout.write('    (\'%s\', [' % app_label)
            for model in app['models']:
                self.stdout.write('        \'%s\',' % model['object_name'])
            self.stdout.write('    ]),')

        self.stdout.write(']')
