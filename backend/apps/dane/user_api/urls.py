from django.urls import path
from .views import *

app_name = 'user-api'

urlpatterns = [
    # URL que lista todos los usuarios de SIGESCO.
    path('usuario', UsuarioAPIView.as_view()),
    # URL que sirve para obtener/editar/eliminar cualquier usuario de Sigesco
    path('usuario/<int:pk>', UsuarioAPIViewDetail.as_view()),

    # URL que sirve para obtener/editar/eliminar usuario autenticado de Sigesco
    path('usuario/detalle', UsuarioAutenticadoAPIViewDetail.as_view()),
    # URL que actualiza la contraseña de un usuario de Sigesco
    path('usuario/password', UsuarioActualizarPasswordAPIViewDetail.as_view()),
    

]