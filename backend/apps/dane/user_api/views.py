from django.http import JsonResponse
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status, permissions
from sigesco.custom_response import CustomResponse
from apps.dane.pagination import PaginationHandlerMixin
from apps.dane.models.user import api_sgco_user
from apps.dane.user_api.serializers import UserSerializer, ChangePasswordSerializer

class UsuarioAPIView(APIView, PaginationHandlerMixin):
    """
        Funciones para:
        - Consultar los datos de las usuarios
        - Guardar datos en el modelo usuarios
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS  
    
    def get(self, request, format=None, *args, **kwargs):
        usuario = api_sgco_user.objects.all()
        page = self.paginate_queryset(usuario)
        if page is not None:
            serializer = self.get_paginated_response(UserSerializer(page, many=True).data)
        else:
            serializer = UserSerializer(usuario, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(CustomResponse.HTTP_201_CREATED(), status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UsuarioAPIViewDetail(APIView):
    """
        Función para:
        - Actualizar/Editar/Eliminar cualquier Usuario de SIGESCO.
    """
    permission_classes = [IsAuthenticated]
   
    def get_object(self, pk):
        try:
            return api_sgco_user.objects.get(pk=pk)
        except api_sgco_user.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        usuario = self.get_object(pk)
        serializer = UserSerializer(usuario)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        usuario = self.get_object(request.data)
        serializer = UserSerializer(usuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    # def delete(self, request, pk, format=None):
    #     usuario = self.get_object(pk)
    #     usuario.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)

class UsuarioActualizarPasswordAPIViewDetail(APIView):
    """
        Función para cambiar contraseña
    """
    permission_classes = [IsAuthenticated]
    serializer_class = ChangePasswordSerializer

    def get_object(self, queryset=None):
        return self.request.user

    def put(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            old_password = serializer.data.get("old_password")
            if not self.object.check_password(old_password):
                return Response({"old_password": ["Wrong password."]}, 
                                status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response("Success", status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UsuarioAutenticadoAPIViewDetail(APIView):
    """
        Función para:
        - Actualizar/Editar/Eliminar Usuario autenticado de SIGESCO.
    """
    permission_classes = [IsAuthenticated]
    
    def get_object(self, request):
        try:
            return api_sgco_user.objects.get(username=self.request.user)
        except api_sgco_user.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        usuario = self.get_object(request.user)
        serializer = UserSerializer(usuario)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, format=None):
        usuario = self.get_object(request.data)
        serializer = UserSerializer(usuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    # def delete(self, request, format=None):
    #     usuario = self.get_object(request.user)
    #     usuario.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)

