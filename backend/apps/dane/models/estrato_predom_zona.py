from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

class api_sgco_estr_predom_zona(TimeStampedModel):

    espz_dfcha = models.DateField(verbose_name='Fecha del cálculo')
    espz_izna = models.IntegerField(verbose_name='Zona')
    espz_igrpo_csa = models.IntegerField(verbose_name='Estrato para el grupo casa')
    espz_igrpo_apto = models.IntegerField(verbose_name='Estrato para el grupo apartamento')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Estrato predominante de la zona"
        verbose_name_plural = "Estrato predominante de la zona"
        ordering = ['id']

    def __str__(self):
        return '%s Municipio: %s' % (self.id, self.mpio)

class api_sgco_estrato_atip(TimeStampedModel):

    esat_dfecha = models.DateField(verbose_name='Fecha del cálculo')
    esat_iestrto = models.IntegerField(verbose_name='Estrato atipico')
    esat_iama_1 = models.IntegerField(verbose_name='Atipica más 1')
    esat_iame_1 = models.IntegerField(verbose_name='Atipica menos 1')
    esat_iama_2 = models.IntegerField(verbose_name='Atipica más 2')
    esat_iame_2 = models.IntegerField(verbose_name='Atipica menos 2')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')
    tiva = models.ForeignKey(api_sgco_listas, on_delete=models.CASCADE,  verbose_name='Identificador para el tipo de vivienda atípica')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Estrato atípico"
        verbose_name_plural = "Estrato atípico"
        ordering = ['id']

    def __str__(self):
        return '%s Municipio: %s' % (self.id, self.mpio)

