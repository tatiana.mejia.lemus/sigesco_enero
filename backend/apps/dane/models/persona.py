from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

class api_sgco_persona(TimeStampedModel): 
        
    pers_cnmbre = models.CharField(max_length=100, verbose_name='Nombres')
    pers_cprmer_aplldo = models.CharField(max_length=150, verbose_name='Primer Apellido', blank=True, null=True)
    pers_csgndo_aplldo = models.CharField(max_length=150, verbose_name='Segundo Apellido', blank=True, null=True)
    pers_nidntfccion = models.CharField(max_length=25, verbose_name='Nº de identificacion')
    # Relaciones foreneas
    tiid = models.ForeignKey(api_sgco_listas, related_name='tiid', on_delete=models.CASCADE, verbose_name='Tipo de Identificacion')
    esci = models.ForeignKey(api_sgco_listas, related_name='esci', on_delete=models.CASCADE, verbose_name='Estado civil', blank=True, null=True)
   
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')
    
    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"
        ordering = ['id']

    def __str__(self):
        return '%s %s %s' % (self.pers_cnmbre, self.pers_cprmer_aplldo, self.pers_csgndo_aplldo)
    
    def documentos(self):
        s = str(self.tiid) + " ,Nº de identificacion: " + str(self.pers_nidntfccion)
     
    @property
    def pers_ncdgo(self):
        return self.id