from django.db import models
from django.contrib.auth.models import AbstractUser
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from simple_history.models import HistoricalRecords

class api_sgco_user(AbstractUser):
    mpio = models.OneToOneField(api_sgco_municipio, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Municipio')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
        ordering = ['id']

    def __str__(self):
        return self.username