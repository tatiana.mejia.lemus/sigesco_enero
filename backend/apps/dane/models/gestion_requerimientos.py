import os
from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.validators import *
from datetime import date
from apps.dane.models.user import api_sgco_user
from apps.dane.models.entidad import api_sgco_estr_entidad
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

def path_req_fdcmnto_rspuesta(instance, filename):
    fecha = date.today()
    fecha_path = fecha.strftime("%Y/%m/%d")
    base = str(instance.enti.mpio.mpio_ccdgo)
    folder = "requerimiento/anexos/respuesta/" + fecha_path
    return os.path.join(base, folder, filename)

def path_req_fdcmnto_rdcdo(instance, filename):
    fecha = date.today()
    fecha_path = fecha.strftime("%Y/%m/%d")
    base = str(instance.enti.mpio.mpio_ccdgo)
    folder = "requerimiento/anexos/radicado/" + fecha_path
    return os.path.join(base, folder, filename)

class api_sgco_estr_requerimientos(TimeStampedModel): 

    req_crdcdo = models.CharField(max_length=15, verbose_name='Nº del radicado')
    req_dfcha_ofcio = models.DateField(verbose_name='Fecha del oficio')
    req_dfcha_rdcdo = models.DateField(verbose_name='Fecha del Radicado')
    req_dfcha_asgncion = models.DateField(blank=True, null=True, verbose_name='Fecha de asignacion del soporte')
    req_dfcha_rspuesta = models.DateField(blank=True, null=True, verbose_name='Fecha de Respuesta')
    req_casnto = models.CharField(max_length=50, verbose_name='Asunto del requerimiento')
    req_cnmro_rspuesta = models.CharField(blank=True, null=True, max_length=50, verbose_name='Nº de Respuesta')
    req_cobsrvciones = models.CharField(blank=True, null=True, max_length=250, verbose_name='Observaciones')
    req_fdcmnto_rspuesta = models.FileField(blank=True, null=True, upload_to=path_req_fdcmnto_rspuesta, verbose_name='Documento de respuesta')
    req_fdcmnto_rdcdo = models.FileField(upload_to=path_req_fdcmnto_rdcdo, verbose_name='Documento de radicado', validators=[validate_extension_files, validate_file_size])
    # Relaciones foraneas
    user = models.ForeignKey(api_sgco_user, on_delete=models.CASCADE, verbose_name='Usuario del Dane que asigna el requerimiento')
    enti = models.ForeignKey(api_sgco_estr_entidad, on_delete=models.CASCADE, verbose_name='Entidad')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Requerimiento"
        verbose_name_plural = "Requerimientos"
        ordering = ['id']
    
    def __str__(self):
        return '%s - %s' % (self.user, self.enti)

def path_rean_fanxos(instance, filename):
    fecha = date.today()
    fecha_path = fecha.strftime("%Y/%m/%d")
    base = str(instance.req.enti.mpio.mpio_ccdgo)
    folder = "requerimiento/anexos/" + fecha_path
    return os.path.join(base, folder, filename)

class api_sgco_estr_requerimientos_anexos(TimeStampedModel):
    
    rean_fanxos = models.FileField(upload_to=path_rean_fanxos, verbose_name='Archivos de anexos', validators=[validate_extension_requerimientos_anexos, validate_file_size])
    # Relaciones foraneas
    req = models.ForeignKey(api_sgco_estr_requerimientos, on_delete=models.CASCADE, verbose_name='Requerimiento')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Requerimiento | Anexo"
        verbose_name_plural = "Requerimientos | Anexos"
        ordering = ['id']
    
    def __str__(self):
        return '%s' % (self.req)

class api_sgco_estr_requerimientos_temas(TimeStampedModel):
    
    # Relaciones foraneas
    tema = models.ForeignKey(api_sgco_listas, related_name='tema', on_delete=models.CASCADE, verbose_name='Tema del requerimiento')
    req = models.ForeignKey(api_sgco_estr_requerimientos, on_delete=models.CASCADE, verbose_name='Requerimiento')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Requerimiento | Tema"
        verbose_name_plural = "Requerimientos | Temas"
        ordering = ['id']
    
    def __str__(self):
        return '%s %s' % (self.req, self.tema)

class api_sgco_estr_requerimientos_user(TimeStampedModel):

    # Relaciones foraneas    
    user = models.ForeignKey(api_sgco_user, on_delete=models.CASCADE, verbose_name='Usuario DANE al que se asigna el requerimiento')
    req = models.ForeignKey(api_sgco_estr_requerimientos, on_delete=models.CASCADE, verbose_name='Requerimiento')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Requerimiento | Usuario"
        verbose_name_plural = "Requerimientos | Usuarios"
        ordering = ['id']
    
    def __str__(self):
        return '%s %s' % (self.req, self.user)