from django.db import models
from model_utils.models import TimeStampedModel, SoftDeletableModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.persona import api_sgco_persona
from apps.dane.models.zhg import api_sgco_zhg
from apps.dane.models.uaf import api_sgco_uaf
from apps.dane.models.listas import api_sgco_listas
from apps.dane.validators import *
from simple_history.models import HistoricalRecords

class api_sgco_estr_rgstro1_igac(TimeStampedModel, SoftDeletableModel):

    reg1_cnmro_prdial = models.CharField(max_length=25, verbose_name='Nº predial')
    reg1_inmro_orden = models.IntegerField(verbose_name='Nº de Orden')
    reg1_ittal_rgstros = models.IntegerField(verbose_name='Total de registros a relacionar')
    reg1_cdrccion = models.CharField(max_length=100, verbose_name='Direccion del Predio')
    reg1_icmna = models.CharField(max_length=1, verbose_name='Comuna', blank=True, null=True)
    deec = models.ForeignKey(api_sgco_listas, related_name='deec',  on_delete=models.CASCADE,  verbose_name='Destino economico')  
    # Area
    reg1_narea_trrno = models.FloatField(verbose_name='Area del Terreno')
    reg1_narea_cnstruida = models.FloatField(verbose_name='Area Construida')
    # Avaluo y vigencia
    reg1_navluo = models.FloatField(verbose_name='Avaluo')
    reg1_cvgncia = models.CharField(max_length=8, verbose_name='Vigencia')
    reg1_inmro_prdial_ncional = models.CharField(max_length=30, verbose_name='Nº predial Anterior')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')
    pers = models.ForeignKey(api_sgco_persona, on_delete=models.CASCADE, verbose_name='Representante a estratificar')
    treg = models.ForeignKey(api_sgco_listas, related_name='treg', on_delete=models.CASCADE, verbose_name='Tipo de registro catastral')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Registro 1 IGAC"
        verbose_name_plural = "Registro 1 IGAC"
        ordering = ['id']

    def __str__(self):
        return 'Predial: %s Orden: %s' % (self.reg1_cnmro_prdial, self.reg1_inmro_orden)
    

