import os
from django.db import models
from model_utils.models import TimeStampedModel
from model_utils import Choices
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.persona import api_sgco_persona
from apps.dane.validators import validate_file_size
from apps.dane.models.zhg import api_sgco_zhg
from apps.dane.models.uaf import api_sgco_uaf
from apps.dane.models.listas import api_sgco_listas
from apps.dane.validators import *
from simple_history.models import HistoricalRecords

def path_estr_fdcmntos(instance, filename):
    base = str(instance.mpio.mpio_ccdgo)
    folder = "estratificacion/documentos"
    return os.path.join(base, folder, filename)

class api_sgco_estr(TimeStampedModel):

    estr_crta = models.CharField(max_length=100, verbose_name='Ruta', blank=True, null=True)
    estr_iorden_rcrrido = models.IntegerField(verbose_name='Nº de orden de recorrido', blank=True, null=True)  
    estr_cvrda = models.CharField(max_length=50, verbose_name='Vereda', blank=True, null=True)
    estr_crclctor = models.CharField(max_length=80, verbose_name='Persona que recolecta información formulario', blank=True, null=True)
    estr_dfcha_rgstro = models.DateField(verbose_name='Fecha de Registro del formulario')
    # Números prediales
    estr_inmro_prdial = models.CharField(max_length=25, verbose_name='Número predial Antiguo')
    estr_inmro_prdial_ncional = models.CharField(max_length=30, verbose_name='Número predial nacional')
    estr_inmro_prdial_tmpral = models.CharField(max_length=25, verbose_name='Número predial temporal', blank=True, null=True)
    # Dirección y Documentos
    estr_nzna = models.IntegerField(verbose_name='Nº de zona', blank=True, null=True)
    estr_cdrccion_prdio = models.CharField(max_length=100, verbose_name='Direccion del Predio')
    estr_btiene_vvienda = models.BooleanField(default = False, verbose_name='El Predio Tiene Vivienda', blank=True, null=True)
    estr_icntdad_vviendas = models.IntegerField(verbose_name='¿Cuantas viviendas?', blank=True, null=True)
    estr_farea_dmstrda_mtros = models.FloatField(verbose_name='Area demostrada', blank=True, null=True)
    estr_farea_dmstrda_hctareas = models.FloatField(verbose_name='Hectareas demostradas', blank=True, null=True)
    estr_fdcmntos = models.FileField(upload_to=path_estr_fdcmntos, verbose_name='Documento que demuestra el area del predio', validators=[validate_file_size, validate_extension_files], blank=True, null=True)
    estr_navluo = models.FloatField(verbose_name='Valor del Avaluo', blank=True, null=True)
    estr_cdscrpcion_mtvo_vsta = models.CharField(max_length=150,  verbose_name='Descripción del motivo de la visita', blank=True, null=True)
    estr_iestrto = models.IntegerField(verbose_name='Estrato', blank=True, null=True)
    estr_cvgncia = models.CharField(max_length=8, verbose_name='Vigencia', blank=True, null=True)
    # Relaciones foraneas
    movi = models.ForeignKey(api_sgco_listas, related_name='movi', on_delete=models.CASCADE, verbose_name='Motivo de la visita', blank=True, null=True)
    tiex = models.ForeignKey(api_sgco_listas, related_name='tiex', on_delete=models.CASCADE, verbose_name='Tipo de Exclusion')
    time = models.ForeignKey(api_sgco_listas, related_name='time', on_delete=models.CASCADE, verbose_name='Tipo de metodologia')
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')
    pers = models.ForeignKey(api_sgco_persona, on_delete=models.CASCADE, verbose_name='Representante a estratificar')
    esfo = models.ForeignKey(api_sgco_listas, related_name='esfo', on_delete=models.CASCADE, verbose_name='Estado Formulario')
    ties = models.ForeignKey(api_sgco_listas, related_name='ties', on_delete=models.CASCADE, verbose_name='Tipo de estratificación', blank=True, null=True)

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Estratificación"
        verbose_name_plural = "Estratificación"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.estr_iorden_rcrrido)
    
    def propietario_datos(self):
        duenio = str(self.pers) + ' Numero de documento: ' + str(self.pers.documentos)

class api_sgco_estr_area_predio(TimeStampedModel):

    esap_izna = models.IntegerField(blank=True, null=True,  verbose_name='Nº de zona')
    esap_nhctreas = models.FloatField(verbose_name='Hectareas')
    esap_nmtros = models.FloatField(verbose_name='Metros')
    estr = models.ForeignKey(api_sgco_estr, on_delete=models.CASCADE, verbose_name='Predio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')
    
    class Meta:
        verbose_name = "Área del predio"
        verbose_name_plural = "Área de los predios"
        ordering = ['id']

    def __str__(self):
        return '%s : Zona N: %s Hectareas: %s Metros(%s)' % (self.estr, self.esap_izna, self.esap_nhctreas, self.esap_nmtros)

class api_sgco_estr_caracter_vivienda(TimeStampedModel):

    # Vivienda datos y jefe de Vivienda
    ecsv_narea_vvienda = models.FloatField(verbose_name='Area de vivienda')
    ecsv_inmro_hbtcions = models.IntegerField(verbose_name='Nº de habitaciones')
    ecsv_cjfe_vvienda = models.CharField(max_length=150,  verbose_name='Jefe de la vivienda', blank=True, null=True)
    # Contadores luz/agua/gas
    escv_btiene_cntdor_enrgia = models.BooleanField(default = False, verbose_name='La vivienda tiene contador de Energia')
    escv_cnmro_cntdor_enrgia = models.CharField(max_length=50, blank=True, null=True,  verbose_name='Nº de contador de energia')
    escv_btiene_cntdor_acuedcto = models.BooleanField(default = False, verbose_name='La vivienda tiene contador de Acueducto')
    escv_cnmro_cntdor_acuedcto = models.CharField(max_length=50, blank=True, null=True,  verbose_name='Nº de contador de acueducto')
    escv_btiene_cntdor_gas = models.BooleanField(default = False, verbose_name='La vivienda tiene contador de Gas')
    escv_cnmro_cntdor_gas = models.CharField(max_length=50, blank=True, null=True,  verbose_name='Nº de contador de gas')
    # Relaciones foreneas y puntaje
    escv_ipntje = models.IntegerField(blank=True, null=True, verbose_name='Puntaje')
    escv_cobsrvciones = models.CharField(max_length=250, blank=True, null=True,  verbose_name='Observaciones')
    tpvi = models.ForeignKey(api_sgco_listas, on_delete=models.CASCADE, verbose_name='Tipo de Vivienda')
    estr = models.ForeignKey(api_sgco_estr, on_delete=models.CASCADE, verbose_name='Vivienda')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')
    
    class Meta:
        verbose_name = "Característica de la vivienda"
        verbose_name_plural = "Características de las viviendas"
        ordering = ['id']

    def __str__(self):
        if self.escv_ipntje == None:
            return '%s : Puntaje: %s por calcular' % (self.estr)
        else:
            return '%s : Puntaje: %s' % (self.estr, self.escv_ipntje)

def path_vies_ffto_armzon(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "estructura/armazon"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_vies_ffto_cbierta(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "estructura/cubierta"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_vies_ffto_mros(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "estructura/muros"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_vies_ffto_cnsrvcion(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "estructura/conservacion"
    return os.path.join(base, folder, vivienda, folder_interno, filename)


class api_sgco_vivienda_estructura(TimeStampedModel):

    vies_ffto_armzon = models.ImageField(upload_to=path_vies_ffto_armzon, verbose_name='Foto de la estructura: Armazón', validators=[validate_extension_images, validate_image_size])
    vies_ffto_cbierta = models.ImageField(upload_to=path_vies_ffto_cbierta, verbose_name='Foto de la estructura: Cubierta', validators=[validate_extension_images, validate_image_size])
    vies_ffto_mros = models.ImageField(upload_to=path_vies_ffto_mros, verbose_name='Foto de la estructura: Muros', validators=[validate_extension_images, validate_image_size])
    vies_ffto_cnsrvcion = models.ImageField(upload_to=path_vies_ffto_cnsrvcion, verbose_name='Foto de la estructura: Estado conservación', validators=[validate_extension_images, validate_image_size])
    # Relaciones foraneas
    escv = models.OneToOneField(api_sgco_estr_caracter_vivienda, on_delete=models.CASCADE, verbose_name='Caracteristicas de la vivienda')
    vies_iarmzon = models.ForeignKey(api_sgco_listas, related_name='vies_iarmzon', on_delete=models.CASCADE, verbose_name='Estructura - Armazón')
    vies_icbierta = models.ForeignKey(api_sgco_listas, related_name='vies_icbierta', on_delete=models.CASCADE, verbose_name='Estructura - Cubierta')
    vies_imros = models.ForeignKey(api_sgco_listas, related_name='vies_imros', on_delete=models.CASCADE, verbose_name='Estructura - Muros')
    vies_icnsrvcion = models.ForeignKey(api_sgco_listas, related_name='vies_icnsrvcion', on_delete=models.CASCADE, verbose_name='Estructura - Estado Conservación')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Estructura de la vivienda"
        verbose_name_plural = "Estructuras de las vivienda"
        ordering = ['id']

    def __str__(self):
        return '%s: Conservacion: %s' % (self.escv, self.vies_icnsrvcion)

def path_viap_ffto_mros(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "acabados/muros"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_viap_ffto_psos(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "acabados/pisos"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_viap_ffto_fchda(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "acabados/fachada"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

def path_viap_ffto_cnsrvcion(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "acabados/conservacion"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

class api_sgco_vviend_acbdos_prncples(TimeStampedModel):
    
    viap_ffto_mros = models.ImageField(upload_to=path_viap_ffto_mros, verbose_name='Foto de los muros', validators=[validate_extension_images, validate_image_size])
    viap_ffto_psos = models.ImageField(upload_to=path_viap_ffto_psos, verbose_name='Fotos de los pisos', validators=[validate_extension_images, validate_image_size])
    viap_ffto_fchda = models.ImageField(upload_to=path_viap_ffto_fchda, verbose_name='Fotos de la Fachada', validators=[validate_extension_images, validate_image_size])
    viap_ffto_cnsrvcion = models.ImageField(upload_to=path_viap_ffto_cnsrvcion, verbose_name='Foto de la conservacion', validators=[validate_extension_images, validate_image_size])
    # Relaciones foraneas
    escv = models.OneToOneField(api_sgco_estr_caracter_vivienda, on_delete=models.CASCADE, verbose_name='Caracteristicas de la vivienda')
    viap_imros = models.ForeignKey(api_sgco_listas, related_name='viap_imros', on_delete=models.CASCADE, verbose_name='Acabados - Muros')
    viap_ipsos = models.ForeignKey(api_sgco_listas, related_name='viap_ipsos', on_delete=models.CASCADE, verbose_name='Acabados - Pisos')
    viap_ifchda = models.ForeignKey(api_sgco_listas, related_name='viap_ifchda', on_delete=models.CASCADE, verbose_name='Acabados - Fachada')
    viap_icnsrvcion = models.ForeignKey(api_sgco_listas, related_name='viap_icnsrvcion', on_delete=models.CASCADE, verbose_name='Acabados - Estado Conservación')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Acabados principales"
        verbose_name_plural = "Acabados principales"
        ordering = ['id']

    def __str__(self):
        return '%s: Acabados: %s Fachada: %s' % (self.escv, self.viap_imros, self.viap_ifchda)

def path_viba_ffto(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "banio"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

class api_sgco_vivienda_banio(TimeStampedModel):

    viba_ffto = models.ImageField(upload_to=path_viba_ffto, verbose_name='Foto del baño', validators=[validate_extension_images, validate_image_size])
    # Relaciones foraneas
    escv = models.OneToOneField(api_sgco_estr_caracter_vivienda, on_delete=models.CASCADE, verbose_name='Caracteristicas de la vivienda')
    viba_itmno = models.ForeignKey(api_sgco_listas, related_name='viba_itmno', on_delete=models.CASCADE, verbose_name='Baños - Tamaño')
    viba_ienchpe = models.ForeignKey(api_sgco_listas, related_name='viba_ienchpe', on_delete=models.CASCADE, verbose_name='Acabados - Enchape')
    viba_imbliario = models.ForeignKey(api_sgco_listas, related_name='viba_imbliario', on_delete=models.CASCADE, verbose_name='Acabados - Mobiliario')
    viba_icnsrvcion = models.ForeignKey(api_sgco_listas, related_name='viba_icnsrvcion', on_delete=models.CASCADE, verbose_name='Estado del baño')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Baño de la vivienda"
        verbose_name_plural = "Baños de la vivienda"
        ordering = ['id']

    def __str__(self):
        return '%s: Mobiliario: %s Estado del baño: %s' % (self.escv, self.viba_imbliario, self.viba_icnsrvcion)

def path_vico_ffto(instance, filename):
    base = str(instance.escv.estr.mpio.mpio_ccdgo)
    folder = "viviendas" 
    vivienda = str(instance.escv.estr.id)
    folder_interno = "cocina"
    return os.path.join(base, folder, vivienda, folder_interno, filename)

class api_sgco_vivienda_cocina(TimeStampedModel):
    
    vico_ffto = models.ImageField(upload_to=path_vico_ffto, verbose_name='Foto de la cocina', validators=[validate_extension_images, validate_image_size])
    # Relaciones foraneas
    escv = models.OneToOneField(api_sgco_estr_caracter_vivienda, on_delete=models.CASCADE, verbose_name='Caracteristicas de la vivienda')
    vico_itmno = models.ForeignKey(api_sgco_listas, related_name='vico_itmno', on_delete=models.CASCADE, verbose_name='Cocina - Tamaño')
    vico_ienchpe = models.ForeignKey(api_sgco_listas, related_name='vico_ienchpe', on_delete=models.CASCADE, verbose_name='Cocina - Enchape')
    vico_imbliario = models.ForeignKey(api_sgco_listas, related_name='vico_imbliario', on_delete=models.CASCADE, verbose_name='Cocina - Mobiliario')
    vico_icnsrvcion = models.ForeignKey(api_sgco_listas, related_name='vico_icnsrvcion', on_delete=models.CASCADE, verbose_name='Estado de la cocina')
    
    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Cocina de la vivienda"
        verbose_name_plural = "Cocina de la vivienda"
        ordering = ['id']

    def __str__(self):
        return '%s: Mobiliario: %s Estado de la cocina: %s' % (self.escv, self.viba_imbliario, self.viba_icnsrvcion)