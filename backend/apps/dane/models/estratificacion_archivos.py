import os
from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.listas import api_sgco_listas
from apps.dane.validators import *
from simple_history.models import HistoricalRecords

def path_arpr_farchvo_prcso(instance, filename):
    base = str(instance.mpio.mpio_ccdgo)
    folder = "estratificacion/archivos"
    return os.path.join(base, folder, filename)

class api_sgco_archivos_proceso_estr(TimeStampedModel):

    arpr_farchvo_prcso = models.FileField(upload_to=path_arpr_farchvo_prcso, verbose_name='Archivos de proceso de estratificación', validators=[validate_extension_text])
    # Relaciones foraneas
    tiar = models.ForeignKey(api_sgco_listas, related_name='tiar', on_delete=models.CASCADE, verbose_name='Tipo de archivo')
    arpr_dfcha_vgncia = models.DateField(verbose_name='Fecha de la vigencia', blank=True, null=True)
    espr = models.ForeignKey(api_sgco_listas, related_name='espr', on_delete=models.CASCADE, verbose_name='Estado del proceso')
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Municipio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Archivos del proceso de estratificación"
        verbose_name_plural = "Archivos del proceso de estratificación"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.id)