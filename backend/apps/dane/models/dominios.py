from django.db import models
from model_utils.models import TimeStampedModel

class api_sgco_dominios(TimeStampedModel):
    
    domi_cdscrpcion = models.CharField(max_length=100, verbose_name='Descripción para los dominios del sistema')
    domi_bactvo = models.BooleanField(default = True, verbose_name='El dominio se encuentra activo')
    
    class Meta:
        verbose_name = "Dominio"
        verbose_name_plural = "Dominios"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.domi_cdscrpcion)