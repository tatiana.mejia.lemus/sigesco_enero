from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from simple_history.models import HistoricalRecords

class api_sgco_zhg(TimeStampedModel):

    zhg_icdgo = models.IntegerField(verbose_name='Codigo de la zona homogénea geoeconónica')
    zhg_navluo_hctrea = models.FloatField(verbose_name='Avaluo por hectarea')
    zhg_narea =  models.FloatField(verbose_name='Area')
    zhg_navluo_ttal_zna = models.FloatField(max_length=150, blank=True, null=True,  verbose_name='Avaluo Total')
    zhg_nuaf_znal = models.FloatField(blank=True, null=True,  verbose_name='Uaf Zonal')
    zhg_dfcha_rgstro_zhg = models.DateField(verbose_name='Fecha de registro')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "ZHG"
        verbose_name_plural = "ZHG"
        ordering = ['id']

    def __str__(self):
        return 'Cod ZHG: %s Avaluo: %s' % (self.zhg_icdgo, self.zhg_navluo_hctrea)