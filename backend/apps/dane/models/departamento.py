from django.db import models
from model_utils.models import TimeStampedModel

class api_sgco_departamento(TimeStampedModel):
    
    dpto_ccdgo = models.CharField(max_length=2,  verbose_name='Identificador del departamento')
    dpto_cnmbre = models.CharField(max_length=150,  verbose_name='Nombre del departamento')
    
    class Meta:
        verbose_name = "Departamento"
        verbose_name_plural = "Departamentos"
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.dpto_ccdgo, self.dpto_cnmbre)