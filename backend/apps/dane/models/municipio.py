from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.departamento import api_sgco_departamento

class api_sgco_municipio(TimeStampedModel):

    mpio_cnmbre = models.CharField(max_length=150, verbose_name='Nombre')
    mpio_ccdgo = models.CharField(max_length=150, verbose_name='Codigo')
    # Relaciones foraneas
    dpto = models.ForeignKey(api_sgco_departamento, on_delete=models.CASCADE, verbose_name='Departamento')
    
    class Meta:
        verbose_name = "Municipio"
        verbose_name_plural = "Municipios"
        ordering = ['id']

    def __str__(self):
        return '%s %s' % (self.mpio_ccdgo, self.mpio_cnmbre)
