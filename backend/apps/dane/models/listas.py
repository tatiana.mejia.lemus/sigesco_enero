import os
from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.dominios import api_sgco_dominios

def path_lsta_cimgen_rfrncia(instance, filename):
    return os.path.join("imagenes", filename)
    
class api_sgco_listas(TimeStampedModel):
    
    lsta_cdscrpcion = models.CharField(max_length=100, verbose_name='Descripción para la opción de la lista del dominio')
    lsta_iiditem = models.IntegerField(blank=True, null=True,  verbose_name='Valor de la opción del item de la lista')
    lsta_cabrviatra = models.CharField(max_length=5, blank=True, null=True, verbose_name='Abreviatura del item de la lista')
    lsta_bactvo = models.BooleanField(default = True, verbose_name='El valor de la lista se encuentra activo')
    lsta_cimgen_rfrncia = models.ImageField(upload_to=path_lsta_cimgen_rfrncia, blank=True, null=True, verbose_name='Imágen de referencia')
    # Relaciones foraneas
    domi = models.ForeignKey(api_sgco_dominios, on_delete=models.CASCADE, verbose_name='Dominios')
    
    class Meta:
        verbose_name = "Lista"
        verbose_name_plural = "Listas"
        ordering = ['id']

    def __str__(self):
        return '%s: %s: %s' % (self.id, self.domi, self.lsta_cdscrpcion)

