import os
import shutil
from django.dispatch import receiver
from django.conf import settings
from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.validators import *
from simple_history.models import HistoricalRecords

def path_uaf_fcpia_dgtal_aval(instance, filename):
    base = str(instance.mpio.mpio_ccdgo)
    folder = "uaf/aval"
    return os.path.join(base, folder, filename)

class api_sgco_uaf(TimeStampedModel):

    uaf_npm = models.FloatField(verbose_name='Uaf Promedio Municipal')
    uaf_dfcha_aval = models.DateField(verbose_name='Fecha del Aval de la Uafpm')
    uaf_cvgncia_ctstro_rral = models.IntegerField(verbose_name='Vigencia Catastral Rural')
    uaf_csstma_pcuario_prdmnnte = models.CharField(max_length=50,  verbose_name='Sistema pecuario predominante')
    uaf_csstma_agrcla_prdmnnte = models.CharField(max_length=50,  verbose_name='Sistema agricola predominante')
    uaf_cvccion_fnal = models.CharField(max_length=50,  verbose_name='Vocacion Final')
    uaf_fcpia_dgtal_aval = models.FileField(upload_to=path_uaf_fcpia_dgtal_aval,  verbose_name='Copia Digital del Aval')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "UAF"
        verbose_name_plural = "UAF"
        ordering = ['id']
    
    def __str__(self):
        return '%s %s' % (self.mpio, self.uaf_npm)

@receiver(models.signals.pre_save, sender=api_sgco_uaf)
def api_sgco_uaf_auto_delete_file_on_change(sender, instance, **kwargs):
    """
        Archivo antiguo eliminado del sistema de archivos 
        cuando el objeto `MediaFile` correspondiente 
        se actualiza con un archivo nuevo.
    """
    if not instance.pk:
        return False

    try:
        old_file = api_sgco_uaf.objects.get(pk=instance.pk).uaf_fcpia_dgtal_aval
    except api_sgco_uaf.DoesNotExist:
        return False

    new_file = instance.uaf_fcpia_dgtal_aval
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)

def path_uafd_fdcmnto(instance, filename):
    base = str(instance.uaf.mpio.mpio_ccdgo)
    folder = "uaf/documentos"
    return os.path.join(base, folder, filename)

class api_sgco_uaf_documentos(TimeStampedModel):

    uafd_fdcmnto = models.FileField(upload_to=path_uafd_fdcmnto, verbose_name='Documentos uaf', validators=[validate_file_size])
    # Relaciones foraneas
    uaf = models.ForeignKey(api_sgco_uaf, on_delete=models.CASCADE, verbose_name='Identificador de la Uafpm')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "UAF Documentos"
        verbose_name_plural = "UAF Documentos"
        ordering = ['id']
    
    def __str__(self):
        return '%s %s' % (self.uaf, self.uafd_fdcmnto)

    
    

