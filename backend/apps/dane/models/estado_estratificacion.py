from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

class api_sgco_estr_estado_urbano(TimeStampedModel):

    eses_idvpla_nmro = models.IntegerField(verbose_name='Divipola')
    esur_cdcrto_urbno_vgnte = models.CharField(max_length=100, verbose_name='Decreto Urbano Vigente')
    esur_dultmo_plazo = models.DateField(verbose_name='Ultimo plazo')
    esur_cdcrto_adpcion = models.CharField(max_length=250, verbose_name='Decreto de Adopcion')
    esur_cdcrto_cntros_pbldos = models.CharField(max_length=250, verbose_name='Decreto centros Poblados')
    esur_cdcrto_cp_cmncciones = models.CharField(max_length=500, verbose_name='Decreto de Adopcion')
    esur_crvsion_gnral = models.CharField(max_length=250, verbose_name='Revision general', blank=True, null=True)
    esur_crprte_asntmientos_rrales = models.CharField(max_length=500, verbose_name='Reporte de asentamientos rurales')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')
    tpo = models.ForeignKey(api_sgco_listas, related_name='tpo', on_delete=models.CASCADE, verbose_name='Tipo')
    estdo = models.ForeignKey(api_sgco_listas, related_name='estdo', on_delete=models.CASCADE, verbose_name='Estado')
    etpa = models.ForeignKey(api_sgco_listas, related_name='etpa', on_delete=models.CASCADE, verbose_name='Etapa del proceso Rg en el que se encuentra')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Estado estratificación"
        verbose_name_plural = "Estado estratificación"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.id)
