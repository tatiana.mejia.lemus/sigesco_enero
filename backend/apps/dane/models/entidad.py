from django.db import models
from model_utils.models import TimeStampedModel
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

class api_sgco_estr_entidad(TimeStampedModel):

    enti_cnmbre = models.CharField(max_length=100, verbose_name='Nombre de la entidad')
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Municipio')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')
    
    class Meta:
        verbose_name = "Entidad"
        verbose_name_plural = "Entidades"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.enti_cnmbre)

class api_sgco_entidad_remitente(TimeStampedModel):

    usen_cnmbre = models.CharField(max_length=100, verbose_name='Nombre del remitente')
    crgo = models.ForeignKey(api_sgco_listas, related_name='usen_ccrgo', on_delete=models.CASCADE, verbose_name='Cargo') 
    enti = models.ForeignKey(api_sgco_estr_entidad, on_delete=models.CASCADE, verbose_name='Entidad')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')
    
    class Meta:
        verbose_name = "Entidad Remitente"
        verbose_name_plural = "Entidades Remitentes"
        ordering = ['id']

    def __str__(self):
        return '%s: %s' % (self.id, self.usen_cnmbre)

