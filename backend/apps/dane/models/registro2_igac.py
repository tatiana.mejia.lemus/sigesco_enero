from django.db import models
from model_utils.models import TimeStampedModel, SoftDeletableModel
from model_utils import Choices
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.persona import api_sgco_persona
from apps.dane.models import api_sgco_user
from apps.dane.models.zhg import api_sgco_zhg
from apps.dane.models.uaf import api_sgco_uaf
from apps.dane.models.listas import api_sgco_listas
from simple_history.models import HistoricalRecords

class api_sgco_estr_rgstro2_igac(TimeStampedModel, SoftDeletableModel):

    reg2_cnmro_prdial = models.CharField(max_length=25, verbose_name='Nº predial')
    reg2_ittal_rgstros = models.IntegerField(verbose_name='Total de registros')
    reg2_cvgncia = models.CharField(max_length=8, verbose_name='Vigencia')
    # Relaciones foraneas
    mpio = models.ForeignKey(api_sgco_municipio, on_delete=models.CASCADE, verbose_name='Departamento y Municipio')    
    treg = models.ForeignKey(api_sgco_listas, on_delete=models.CASCADE, verbose_name='Tipo de registro catastral')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Registro 2 IGAC"
        verbose_name_plural = "Registro 2 IGAC"
        ordering = ['id']

    def __str__(self):
        return 'Predial: %s Orden: %s' % (self.reg2_cnmro_prdial, self.reg2_inmro_orden)

class api_sgco_estr_igac_cnstrcciones(TimeStampedModel):

    cons_ihbtciones = models.IntegerField(verbose_name='Nº de habitaciones zonas registradas')
    cons_ipntje = models.IntegerField(verbose_name='Puntaje para la estratificacion del registro 2')
    cons_narea_cnstruida = models.FloatField(verbose_name='Area construida', blank=True, null=True)
    cons_inmro_orden = models.IntegerField(verbose_name='Nº de Orden')
    # Relaciones foraneas
    reg2 = models.ForeignKey(api_sgco_estr_rgstro2_igac, on_delete=models.CASCADE, verbose_name='Registro 2')
    uso = models.ForeignKey(api_sgco_listas, on_delete=models.CASCADE, verbose_name='Tipo de uso')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "IGAC Construcciones"
        verbose_name_plural = "IGAC Construcciones"
        ordering = ['id']

    def __str__(self):
        return '%s %s' % (self.id, self.cons_ipntje)


class api_sgco_estr_rgstro2_igac_znas(TimeStampedModel):

    eszn_narea_trrno = models.FloatField(verbose_name='Area del terreno', blank=True, null=True)
    eszn_izna_ecnmca = models.IntegerField(verbose_name='Zona economica')
    eszn_inmro_orden = models.IntegerField(verbose_name='Nº de Orden')
    # Relaciones foraneas
    reg2 = models.ForeignKey(api_sgco_estr_rgstro2_igac, on_delete=models.CASCADE, verbose_name='Registro 2')

    # Campo de seguimiento para el modulo de auditoria
    history = HistoricalRecords(app='auditoria')

    class Meta:
        verbose_name = "Registro 2 IGAC Zonas"
        verbose_name_plural = "Registro 2 IGAC Zonas"

    def __str__(self):
        return '%s %s' % (self.id, self.reg2)