from django.contrib import admin
from apps.dane.models.departamento import api_sgco_departamento
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.models.dominios import api_sgco_dominios
from apps.dane.models.listas import api_sgco_listas
from apps.dane.models.estrato_predom_zona import api_sgco_estr_predom_zona, api_sgco_estrato_atip
from apps.dane.models.user import api_sgco_user
from apps.dane.models.persona import api_sgco_persona
from apps.dane.models.zhg import api_sgco_zhg
from apps.dane.models.uaf import api_sgco_uaf, api_sgco_uaf_documentos
from apps.dane.models.estratificacion import *
from apps.dane.models.registro1_igac import api_sgco_estr_rgstro1_igac
from apps.dane.models.registro2_igac import api_sgco_estr_rgstro2_igac, api_sgco_estr_igac_cnstrcciones, api_sgco_estr_rgstro2_igac_znas
from apps.dane.models.gestion_requerimientos import api_sgco_estr_requerimientos, api_sgco_estr_requerimientos_anexos, api_sgco_estr_requerimientos_temas, api_sgco_estr_requerimientos_user
from apps.dane.models.entidad import api_sgco_estr_entidad, api_sgco_entidad_remitente
from django.contrib.auth.admin import UserAdmin

# Filtro y crud dashboard para user
class api_sgco_userAdmin(UserAdmin):
    list_display = ('username','first_name', 'last_name', 'mpio')
    list_filter = ('username','first_name', 'last_name', 'mpio')
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('first_name', 'last_name', 'mpio')}),
    )

admin.site.register(api_sgco_user, api_sgco_userAdmin)

# Filtro para departamento

class api_sgco_departamentoAdmin(admin.ModelAdmin):
    list_display = ('dpto_cnmbre', 'dpto_ccdgo', 'created')
    list_filter = ('dpto_cnmbre', 'dpto_ccdgo', 'created')

admin.site.register(api_sgco_departamento, api_sgco_departamentoAdmin)

# Filtro para municipio

class api_sgco_municipioAdmin(admin.ModelAdmin):
    list_display = ('dpto', 'mpio_cnmbre', 'mpio_ccdgo')
    list_filter = ('dpto', 'mpio_cnmbre', 'mpio_ccdgo')

admin.site.register(api_sgco_municipio, api_sgco_municipioAdmin)

# Filtro y crud dashboard para persona
class api_sgco_personaAdmin(admin.ModelAdmin):
    list_display = ('pers_cnmbre','pers_cprmer_aplldo', 'pers_csgndo_aplldo', 'pers_nidntfccion', 'tiid', 'esci')
    list_filter = ('pers_cnmbre','pers_cprmer_aplldo', 'pers_csgndo_aplldo', 'pers_nidntfccion', 'tiid', 'esci')

admin.site.register(api_sgco_persona, api_sgco_personaAdmin)

# Filtro y crud dashboard para dominios
class api_sgco_dominiosAdmin(admin.ModelAdmin):
    list_display = ('created','domi_cdscrpcion')
    list_filter = ('created','domi_cdscrpcion')

admin.site.register(api_sgco_dominios, api_sgco_dominiosAdmin)

# Filtro y crud dashboard para listas de dominios
class api_sgco_listasAdmin(admin.ModelAdmin):
    list_display = ('created','lsta_cdscrpcion', 'domi_id')
    list_filter = ('created','lsta_cdscrpcion', 'domi_id')

admin.site.register(api_sgco_listas, api_sgco_listasAdmin)

# Filtro y crud dashboard para zhg
class api_sgco_zhgAdmin(admin.ModelAdmin):
    list_display = ('created','zhg_icdgo', 'zhg_navluo_hctrea', 'zhg_narea', 'zhg_navluo_ttal_zna', 'zhg_nuaf_znal', 'mpio')
    list_filter = ('created','zhg_icdgo', 'zhg_navluo_hctrea', 'zhg_narea', 'zhg_navluo_ttal_zna', 'zhg_nuaf_znal', 'mpio')

admin.site.register(api_sgco_zhg, api_sgco_zhgAdmin)

# Filtro y crud dashboard para Uaf
class api_sgco_uafAdmin(admin.ModelAdmin):
    list_display = ('created','uaf_npm', 'uaf_dfcha_aval', 'uaf_cvgncia_ctstro_rral', 'uaf_csstma_pcuario_prdmnnte', 'uaf_csstma_agrcla_prdmnnte', 'uaf_cvccion_fnal', 'uaf_fcpia_dgtal_aval', 'mpio')
    list_filter = ('created','uaf_npm', 'uaf_dfcha_aval', 'uaf_cvgncia_ctstro_rral', 'uaf_csstma_pcuario_prdmnnte', 'uaf_csstma_agrcla_prdmnnte', 'uaf_cvccion_fnal', 'uaf_fcpia_dgtal_aval', 'mpio')

admin.site.register(api_sgco_uaf, api_sgco_uafAdmin)

class api_sgco_uaf_documentosAdmin(admin.ModelAdmin):
    list_display = ('created','uaf', 'uafd_fdcmnto')
    list_filter = ('created','uaf', 'uafd_fdcmnto')

admin.site.register(api_sgco_uaf_documentos, api_sgco_uaf_documentosAdmin)

# Filtro y crud dashboard para Estratificacion predio
class api_sgco_estrAdmin(admin.ModelAdmin):
    list_display = ('created', 'estr_crta', 'estr_iorden_rcrrido', 'estr_cvrda', 'estr_nzna', 'estr_cdrccion_prdio', 'estr_btiene_vvienda', 
    'estr_icntdad_vviendas', 'estr_fdcmntos', 'estr_navluo', 'estr_cdscrpcion_mtvo_vsta', 'estr_iestrto', 'movi', 'tiex', 'mpio', 'pers')

    list_filter = ('created', 'estr_crta', 'estr_iorden_rcrrido', 'estr_cvrda', 'estr_nzna', 'estr_cdrccion_prdio', 'estr_btiene_vvienda', 
    'estr_icntdad_vviendas', 'estr_fdcmntos', 'estr_navluo', 'estr_cdscrpcion_mtvo_vsta', 'estr_iestrto', 'movi', 'tiex', 'mpio', 'pers')

admin.site.register(api_sgco_estr, api_sgco_estrAdmin)

# Filtro y crud dashboard para Area Predio
class api_sgco_estr_area_predioAdmin(admin.ModelAdmin):
    list_display = ('created','estr', 'esap_izna', 'esap_nhctreas', 'esap_nmtros')
    list_filter = ('created','estr', 'esap_izna', 'esap_nhctreas', 'esap_nmtros')

admin.site.register(api_sgco_estr_area_predio, api_sgco_estr_area_predioAdmin)


# Filtro y crud dashboard para Caracteristicas de la vivienda
class api_sgco_estr_caracter_viviendaAdmin(admin.ModelAdmin):
    list_display = ('created','tpvi', 'ecsv_narea_vvienda', 'ecsv_cjfe_vvienda', 'escv_btiene_cntdor_enrgia', 'escv_cnmro_cntdor_enrgia', 'escv_btiene_cntdor_acuedcto', 'escv_cnmro_cntdor_acuedcto',
    'escv_btiene_cntdor_gas', 'escv_cnmro_cntdor_gas', 'escv_cobsrvciones', 'estr')

    list_filter = ('created','tpvi', 'ecsv_narea_vvienda', 'ecsv_cjfe_vvienda', 'escv_btiene_cntdor_enrgia', 'escv_cnmro_cntdor_enrgia', 'escv_btiene_cntdor_acuedcto', 'escv_cnmro_cntdor_acuedcto',
    'escv_btiene_cntdor_gas', 'escv_cnmro_cntdor_gas', 'escv_cobsrvciones', 'estr')

admin.site.register(api_sgco_estr_caracter_vivienda, api_sgco_estr_caracter_viviendaAdmin)

# Filtro y crud dashboard para vivienda estructura
class api_sgco_vivienda_estructuraAdmin(admin.ModelAdmin):
    list_display = ('created','escv', 'vies_iarmzon', 'vies_icbierta', 'vies_imros', 'vies_icnsrvcion', 'vies_ffto_armzon', 'vies_ffto_cbierta', 'vies_ffto_mros', 'vies_ffto_cnsrvcion')
    list_filter = ('created','escv', 'vies_iarmzon', 'vies_icbierta', 'vies_imros', 'vies_icnsrvcion', 'vies_ffto_armzon', 'vies_ffto_cbierta', 'vies_ffto_mros', 'vies_ffto_cnsrvcion')

admin.site.register(api_sgco_vivienda_estructura, api_sgco_vivienda_estructuraAdmin)

# Filtro y crud dashboard para vivienda acabados
class api_sgco_vviend_acbdos_prncplesAdmin(admin.ModelAdmin):
    list_display = ('created','escv', 'viap_imros', 'viap_ipsos', 'viap_ifchda', 'viap_icnsrvcion', 'viap_ffto_mros', 'viap_ffto_psos', 'viap_ffto_fchda', 'viap_ffto_cnsrvcion')
    list_filter = ('created','escv', 'viap_imros', 'viap_ipsos', 'viap_ifchda', 'viap_icnsrvcion', 'viap_ffto_mros', 'viap_ffto_psos', 'viap_ffto_fchda', 'viap_ffto_cnsrvcion')

admin.site.register(api_sgco_vviend_acbdos_prncples, api_sgco_vviend_acbdos_prncplesAdmin)

# Filtro y crud dashboard para vivienda baños
class api_sgco_vivienda_banioAdmin(admin.ModelAdmin):
    list_display = ('created','escv', 'viba_itmno', 'viba_ienchpe', 'viba_imbliario', 'viba_icnsrvcion', 'viba_ffto')
    list_filter = ('created','escv', 'viba_itmno', 'viba_ienchpe', 'viba_imbliario', 'viba_icnsrvcion', 'viba_ffto')

admin.site.register(api_sgco_vivienda_banio, api_sgco_vivienda_banioAdmin)

# Filtro y crud dashboard para vivienda cocina
class api_sgco_vivienda_cocinaAdmin(admin.ModelAdmin):
    list_display = ('created','escv', 'vico_itmno', 'vico_ienchpe', 'vico_imbliario', 'vico_icnsrvcion', 'vico_ffto')
    list_filter = ('created','escv', 'vico_itmno', 'vico_ienchpe', 'vico_imbliario', 'vico_icnsrvcion', 'vico_ffto')

admin.site.register(api_sgco_vivienda_cocina, api_sgco_vivienda_cocinaAdmin)

# Filtro y crud dashboard para Estratos Zona predominante
class api_sgco_estr_predom_zonaAdmin(admin.ModelAdmin):
    list_display = ('created','mpio', 'espz_dfcha', 'espz_izna', 'espz_igrpo_csa', 'espz_igrpo_apto')
    list_filter = ('created','mpio', 'espz_dfcha', 'espz_izna', 'espz_igrpo_csa', 'espz_igrpo_apto')

admin.site.register(api_sgco_estr_predom_zona, api_sgco_estr_predom_zonaAdmin)

# Filtro y crud dashboard para Estratos atipicos
class api_sgco_estrato_atipAdmin(admin.ModelAdmin):
    list_display = ('created','mpio', 'tiva', 'esat_dfecha', 'esat_iestrto', 'esat_iama_1', 'esat_iame_1', 'esat_iama_2', 'esat_iame_2')
    list_filter = ('created','mpio', 'tiva', 'esat_dfecha', 'esat_iestrto', 'esat_iama_1', 'esat_iame_1', 'esat_iama_2', 'esat_iame_2')

admin.site.register(api_sgco_estrato_atip, api_sgco_estrato_atipAdmin)

# Filtro y crud dashboard para Registro tipo 1
class api_sgco_estr_rgstro1_igacAdmin(admin.ModelAdmin):
    list_display = ('created','reg1_cnmro_prdial', 'treg', 'reg1_inmro_orden', 'reg1_ittal_rgstros', 'reg1_cdrccion', 'reg1_icmna', 'reg1_narea_trrno',
    'reg1_narea_cnstruida', 'reg1_navluo', 'reg1_cvgncia', 'reg1_inmro_prdial_ncional', 'mpio', 'pers')

    list_filter = ('created','reg1_cnmro_prdial', 'treg', 'reg1_inmro_orden', 'reg1_ittal_rgstros', 'reg1_cdrccion', 'reg1_icmna', 'reg1_narea_trrno',
    'reg1_narea_cnstruida', 'reg1_navluo', 'reg1_cvgncia', 'reg1_inmro_prdial_ncional', 'mpio', 'pers')

admin.site.register(api_sgco_estr_rgstro1_igac, api_sgco_estr_rgstro1_igacAdmin)

# Filtro y crud dashboard para Registro tipo 2
class api_sgco_estr_rgstro2_igacAdmin(admin.ModelAdmin):
    list_display = ('created','reg2_cnmro_prdial', 'treg', 'reg2_ittal_rgstros','mpio')

    list_filter = ('created','reg2_cnmro_prdial', 'treg', 'reg2_ittal_rgstros','mpio')

admin.site.register(api_sgco_estr_rgstro2_igac, api_sgco_estr_rgstro2_igacAdmin)

# Filtro y crud dashboard para construcciones registro 2
class api_sgco_estr_igac_cnstrccionesAdmin(admin.ModelAdmin):
    list_display = ('created','cons_ihbtciones', 'cons_ipntje', 'cons_narea_cnstruida', 'reg2', 'uso')

    list_filter = ('created','cons_ihbtciones', 'cons_ipntje', 'cons_narea_cnstruida', 'reg2', 'uso')

admin.site.register(api_sgco_estr_igac_cnstrcciones, api_sgco_estr_igac_cnstrccionesAdmin)

# Filtro y crud dashboard para Estratificacion de Zonas en Registro tipo 2
class api_sgco_estr_rgstro2_igac_znasAdmin(admin.ModelAdmin):
    list_display = ('created','reg2', 'eszn_narea_trrno', 'eszn_izna_ecnmca')

    list_filter = ('created','reg2', 'eszn_narea_trrno', 'eszn_izna_ecnmca')

admin.site.register(api_sgco_estr_rgstro2_igac_znas, api_sgco_estr_rgstro2_igac_znasAdmin)

# Filtro y crud dashboard para Entidad
class api_sgco_estr_entidadAdmin(admin.ModelAdmin):
    list_display = ('created','enti_cnmbre', 'mpio')

    list_filter = ('created','enti_cnmbre', 'mpio')

admin.site.register(api_sgco_estr_entidad, api_sgco_estr_entidadAdmin)

# Filtro y crud dashboard para Entidad-User(remitente)
class api_sgco_entidad_remitenteAdmin(admin.ModelAdmin):
    list_display = ('created','usen_cnmbre', 'crgo', 'enti')

    list_filter = ('created','usen_cnmbre', 'crgo', 'enti')

admin.site.register(api_sgco_entidad_remitente, api_sgco_entidad_remitenteAdmin)

# Filtro y cru dashboard para requerimientos
class api_sgco_estr_requerimientosAdmin(admin.ModelAdmin):
    list_display = ('created', 'enti', 'req_crdcdo', 'req_dfcha_ofcio', 'req_dfcha_rdcdo', 'req_dfcha_asgncion', 'req_dfcha_rspuesta', 'req_casnto', 
    'req_cnmro_rspuesta', 'req_cobsrvciones', 'req_fdcmnto_rspuesta', 'req_fdcmnto_rdcdo')

    list_filter = ('created', 'enti', 'req_crdcdo', 'req_dfcha_ofcio', 'req_dfcha_rdcdo', 'req_dfcha_asgncion', 'req_dfcha_rspuesta', 'req_casnto', 
    'req_cnmro_rspuesta', 'req_cobsrvciones', 'req_fdcmnto_rspuesta', 'req_fdcmnto_rdcdo')

admin.site.register(api_sgco_estr_requerimientos, api_sgco_estr_requerimientosAdmin)

# Filtro y cru dashboard para requerimientos-anexos
class api_sgco_estr_requerimientos_anexosAdmin(admin.ModelAdmin):
    list_display = ('created','req', 'rean_fanxos')

    list_filter = ('created','req', 'rean_fanxos')

admin.site.register(api_sgco_estr_requerimientos_anexos, api_sgco_estr_requerimientos_anexosAdmin)

# Filtro y cru dashboard para requerimientos-temas
class api_sgco_estr_requerimientos_temasAdmin(admin.ModelAdmin):
    list_display = ('created','req', 'tema')

    list_filter = ('created','req', 'tema')

admin.site.register(api_sgco_estr_requerimientos_temas, api_sgco_estr_requerimientos_temasAdmin)

# Filtro y cru dashboard para requerimientos-usuario a quien se asigna el requerimiento
class api_sgco_estr_requerimientos_userAdmin(admin.ModelAdmin):
    list_display = ('created','req')

    list_filter = ('created','req')

admin.site.register(api_sgco_estr_requerimientos_user, api_sgco_estr_requerimientos_userAdmin)

