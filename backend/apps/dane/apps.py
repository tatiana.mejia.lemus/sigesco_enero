from django.apps import AppConfig


class DaneConfig(AppConfig):
    name = 'dane'
    label = 'dane'
