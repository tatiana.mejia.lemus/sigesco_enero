from django.http import JsonResponse
from django.http import Http404
from django.conf import settings
from django.db.models import Q
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status
from sigesco.custom_response import CustomResponse
from apps.dane.pagination import PaginationHandlerMixin
from apps.dane.uaf_api.serializers import *
from apps.dane.models.uaf import *
from datetime import date

class UAFAPIView(APIView, PaginationHandlerMixin):
    """
        Funciones para:
        - Consultar los datos de la UAF y sus relacionados
        - Guardar datos en el modelo UAF y sus relacionados
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
    
    def get(self, request, format=None, *args, **kwargs):
        
        pecuario = self.request.query_params.get('pecuario', None)
        agricola = self.request.query_params.get('agricola', None)
        municipio = self.request.query_params.get('municipio', None)

        if pecuario == '' and agricola == '' and municipio == '':
            return Response({'error': 'Todos los parametros del request están vacios'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)  
        else:
            if pecuario is not None and agricola is not None and municipio is not None:
                try:
                    uaf = api_sgco_uaf.objects.filter(
                            Q(mpio = request.GET.get('municipio')), 
                            (Q(uaf_csstma_agrcla_prdmnnte__icontains = request.GET.get('agricola')) |
                            Q(uaf_csstma_pcuario_prdmnnte__icontains = request.GET.get('pecuario')))
                        )
                except api_sgco_uaf.DoesNotExist:
                    raise Http404
            elif pecuario is not None and agricola is not None:
                try:
                    uaf = api_sgco_uaf.objects.filter(
                            Q(uaf_csstma_agrcla_prdmnnte__icontains = request.GET.get('agricola')) |
                            Q(uaf_csstma_pcuario_prdmnnte__icontains = request.GET.get('pecuario'))
                    )
                except api_sgco_uaf.DoesNotExist:
                    raise Http404
            elif pecuario is not None and municipio is not None:
                try:
                    uaf = api_sgco_uaf.objects.filter(
                            Q(mpio = request.GET.get('municipio')),
                            Q(uaf_csstma_pcuario_prdmnnte__icontains = request.GET.get('pecuario'))
                    )
                except api_sgco_uaf.DoesNotExist:
                    raise Http404
            elif agricola is not None and municipio is not None:
                try:
                    uaf = api_sgco_uaf.objects.filter(
                            Q(mpio = request.GET.get('municipio')),
                            Q(uaf_csstma_pcuario_prdmnnte__icontains = request.GET.get('agricola'))
                    )
                except api_sgco_uaf.DoesNotExist:
                    raise Http404
            elif pecuario is not None:
                uaf = api_sgco_uaf.objects.filter(uaf_csstma_pcuario_prdmnnte__icontains = request.GET.get('pecuario'))
            elif agricola is not None:
                uaf = api_sgco_uaf.objects.filter(uaf_csstma_agrcla_prdmnnte__icontains = request.GET.get('agricola'))
            elif municipio is not None:
                uaf = api_sgco_uaf.objects.filter(mpio = request.GET.get('municipio'))
            else:        
                uaf = api_sgco_uaf.objects.all()
        
        page = self.paginate_queryset(uaf)
            
        if page is not None:
            serializer = self.get_paginated_response(UafGetSerializer(page, many=True).data)
        else:
            serializer = UafGetSerializer(uaf, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):   
        # Hallar fecha actual 
        hoy = date.today()
        fecha = hoy.strftime("%Y-%m-%d") 

        # Comprobar municipio y fecha, para no permitir el guardado de información el mismo día
        uaf = api_sgco_uaf.objects.filter(Q(mpio=request.data['mpio']), Q(uaf_dfcha_aval=fecha)).count()            
        if uaf < 1:
            serializer = UafSerializer(data=request.data)
            if serializer.is_valid():
                uaf = serializer.save()
                if request.data['uafd_fdcmnto']:
                    documentos = {
                        "uaf": uaf,
                        "uafd_fdcmnto": request.data['uafd_fdcmnto']
                    }
                    serializerDocumentos = UafDocumentosSerializer(data=documentos)
                    list_uafd_fdcmnto = request.FILES.getlist('uafd_fdcmnto')

                    for item in list_uafd_fdcmnto:
                        f = api_sgco_uaf_documentos.objects.create(
                            uaf = uaf,
                            uafd_fdcmnto = item
                        )
                return Response(CustomResponse.HTTP_201_CREATED(), status=status.HTTP_201_CREATED)
                
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error': 'El municipio ya subió información de UAF en esta fecha'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)  

        
class UAFAPIViewDetail(APIView):
    """
        Funciones para:
        - Consultar recurso unico 
        - Actualizar/Editar recurso del modelo UAF
    """

    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return api_sgco_uaf.objects.get(pk=pk)
        except api_sgco_uaf.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        uaf = self.get_object(pk)
        serializer = UafGetSerializer(uaf)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        uaf = self.get_object(pk)

        serializer = UafPutSerializer(uaf, data=request.data)
        if serializer.is_valid():
            serializer.save()
            
            try:
                if request.data['uafd_fdcmnto'] != '':
                    request.data['uafd_fdcmnto']                           
                    # Borrar las referencias de la tabla api_sgco_uaf_documentos     
                    # de documentos adjuntos por uaf      
                    document = api_sgco_uaf_documentos.objects.filter(uaf = uaf)
                    document.delete()

                    # Borrar todos los documentos UAF adjuntos y adiciona los nuevos archivos
                    list_uafd_fdcmnto = request.FILES.getlist('uafd_fdcmnto')

                    base = str(uaf.mpio.mpio_ccdgo)
                    folder = "uaf/documentos"
                    path = os.path.join(settings.MEDIA_ROOT, base, folder)

                    for the_file in os.listdir(path):
                        file_path = os.path.join(path, the_file)
                        try:
                            if os.path.isfile(file_path):
                                os.unlink(file_path)
                        except Exception as e:
                            print(e)

                    for item in list_uafd_fdcmnto:
                        f = api_sgco_uaf_documentos.objects.create(
                            uaf = uaf,
                            uafd_fdcmnto = item
                        )
            except:
                # Imprimir error por consola
                print("La variable -uafd_fdcmnto- no está en el request")    
            return Response(serializer.data, status=status.HTTP_200_OK)
            
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk, format=None):
    #     uaf = self.get_object(pk)
    #     uaf.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)