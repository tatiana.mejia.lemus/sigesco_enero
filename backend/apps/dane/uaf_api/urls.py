from django.urls import path
from .views import *

app_name = 'uaf-api'

urlpatterns = [
    # URL que lista todos los UAF 
    path('uaf', UAFAPIView.as_view()),
    # URL que lee/edita un UAF
    path('uaf/<int:pk>', UAFAPIViewDetail.as_view()),

]