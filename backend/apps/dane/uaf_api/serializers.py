from rest_framework import serializers
from apps.dane.models.uaf import api_sgco_uaf, api_sgco_uaf_documentos
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.municipio_api.serializers import MunicipioSerializer

class UafDocumentosSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_sgco_uaf_documentos
        exclude = ['created', 'modified']

class UafGetSerializer(serializers.ModelSerializer):
    
    api_sgco_uaf_documentos_set = UafDocumentosSerializer(many=True, read_only=True)
    mpio = MunicipioSerializer()

    class Meta:
        model = api_sgco_uaf
        exclude = ['created', 'modified']

class UafSerializer(serializers.ModelSerializer):
    
    api_sgco_uaf_documentos_set = UafDocumentosSerializer(many=True, read_only=True)
    class Meta:
        model = api_sgco_uaf
        exclude = ['created', 'modified']

class UafPutSerializer(serializers.Serializer):
    
    api_sgco_uaf_documentos_set = UafDocumentosSerializer(many=True, read_only=True)
    
    uaf_npm = serializers.FloatField()
    uaf_dfcha_aval = serializers.DateField()
    uaf_cvgncia_ctstro_rral = serializers.IntegerField()
    uaf_csstma_pcuario_prdmnnte = serializers.CharField(max_length=50)
    uaf_csstma_agrcla_prdmnnte = serializers.CharField(max_length=50)
    uaf_cvccion_fnal = serializers.CharField(max_length=50)
    uaf_fcpia_dgtal_aval = serializers.FileField(allow_null=True)
    mpio = serializers.PrimaryKeyRelatedField(queryset=api_sgco_municipio.objects.all())

    def update(self, instance, validated_data):
        instance.uaf_npm = validated_data.get('uaf_npm', instance.uaf_npm)
        instance.uaf_dfcha_aval = validated_data.get('uaf_dfcha_aval', instance.uaf_dfcha_aval)
        instance.uaf_cvgncia_ctstro_rral = validated_data.get('uaf_cvgncia_ctstro_rral', instance.uaf_cvgncia_ctstro_rral)
        instance.uaf_csstma_pcuario_prdmnnte = validated_data.get('uaf_csstma_pcuario_prdmnnte', instance.uaf_csstma_pcuario_prdmnnte)
        instance.uaf_csstma_agrcla_prdmnnte = validated_data.get('uaf_csstma_agrcla_prdmnnte', instance.uaf_csstma_agrcla_prdmnnte)
        instance.uaf_cvccion_fnal = validated_data.get('uaf_cvccion_fnal', instance.uaf_cvccion_fnal)
        if validated_data.get('uaf_fcpia_dgtal_aval') != None:
            instance.uaf_fcpia_dgtal_aval = validated_data.get('uaf_fcpia_dgtal_aval', instance.uaf_fcpia_dgtal_aval)
        instance.mpio = validated_data.get('mpio', instance.mpio)
        instance.save()
        return instance

