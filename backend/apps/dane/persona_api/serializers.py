from rest_framework import serializers
from apps.dane.models.persona import api_sgco_persona
from apps.dane.dominios_api.serializers import *

class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_sgco_persona
        exclude = ['created', 'modified']

class PersonaGetSerializer(serializers.ModelSerializer):
    tiid = ListaSerializer()
    esci = ListaSerializer()

    class Meta:
        model = api_sgco_persona
        exclude = ['created', 'modified']