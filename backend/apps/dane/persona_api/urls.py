from django.urls import path
from .views import *

app_name = 'persona-api'

urlpatterns = [
    # url que lista todas las personas con predio.
    path('persona', PersonaAPIView.as_view()),
    # url que lee/edita o elimina a persona
    path('persona/<int:pk>', PersonaAPIViewDetail.as_view()),

]