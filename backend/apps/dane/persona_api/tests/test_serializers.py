from rest_framework.test import APITestCase

from api.persona_api.serializers import SGCO_PERSONASerializer


class SerializerTests(APITestCase):
    def test_persona_log_serializer_error_if_no_pers_nidntfccion(self):
        data = {
            'pers_cnmbre': 'tatiana',
            'pers_caplldo': 'mejia',
            'pers_nidntfccion': '000000',
            'pers_tidntfccion': 'CC',
            'pers_estcvl': 'Casado/a'
        }

        serializer = SGCO_PERSONASerializer(data=data)
        serializer.is_valid()

        self.assertEqual(serializer.errors['pers_nidntfccion'], ['no es un numero de identificacion valido.'])

    def test_persona_log_serializer_error_if_no_tidntfccion(self):
        data = {
            'pers_cnmbre': 'tatiana',
            'pers_caplldo': 'mejia',
            'pers_nidntfccion': '000000',
            'pers_tidntfccion': 'CC',
            'pers_estcvl': 'Casado/a'
        }

        serializer = SGCO_PERSONASerializer(data=data)
        serializer.is_valid()

        self.assertEqual(serializer.errors['pers_tidntfccion'], ['No es una opcion valida.'])
    
    def test_persona_log_serializer_error_if_no_pers_estcvl(self):
        data = {
            'pers_cnmbre': 'tatiana',
            'pers_caplldo': 'mejia',
            'pers_nidntfccion': '000000',
            'pers_tidntfccion': 'CC',
            'pers_estcvl': 'Casado/a'
        }

        serializer = SGCO_PERSONASerializer(data=data)
        serializer.is_valid()

        self.assertEqual(serializer.errors['pers_estcvl'], ['No es una opcion valida.'])
