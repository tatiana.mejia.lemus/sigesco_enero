from django.urls import path
from .views import *

app_name = 'municipio-api'

urlpatterns = [
    # URL para listar los departamentos (Sin paginación)
    path('departamento', DepartamentoAPIView.as_view()),
    # URL para listar todos los municipios (Sin paginación)
    path('municipio', MunicipioAPIView.as_view()),
    # URL para listar los municipios filtrados por departamento (Sin paginación)
    path('municipio/<int:pk>/departamento', MunicipioAPIViewFilter.as_view()),

    # URL para listar los departamentos (Paginado)
    path('departamento-page', DepartamentoPaginadoAPIView.as_view()),
    # URL para listar todos los municipios (Paginado)
    path('municipio-page', MunicipioPaginadoAPIView.as_view()),
    # URL para listar los municipios filtrados por departamento (Paginado)
    path('municipio-page/<int:pk>/departamento-page', MunicipioPaginadoAPIViewFilter.as_view()),

    # URL para consultar un municipio
    path('municipio/<int:pk>', MunicipioAPIViewDetail.as_view()),
    # URL para consultar un departamento
    path('departamento/<int:pk>', DepartamentoAPIViewDetail.as_view()),

]