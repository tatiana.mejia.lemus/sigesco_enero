from rest_framework import serializers
from apps.dane.models.departamento import api_sgco_departamento
from apps.dane.models.municipio import api_sgco_municipio

class DepartamentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_departamento
        exclude = ['created', 'modified']

class MunicipioSerializer(serializers.ModelSerializer):

    dpto = DepartamentoSerializer()

    class Meta:
        model = api_sgco_municipio
        exclude = ['created', 'modified']

