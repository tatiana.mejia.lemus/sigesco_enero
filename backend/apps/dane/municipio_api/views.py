from django.http import JsonResponse
from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.settings import api_settings
from apps.dane.municipio_api.serializers import DepartamentoSerializer, MunicipioSerializer
from apps.dane.models.departamento import api_sgco_departamento
from apps.dane.models.municipio import api_sgco_municipio
from apps.dane.pagination import PaginationHandlerMixin

class DepartamentoPaginadoAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los departamentos
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS    

    def get(self, request, format=None, *args, **kwargs):
        modelo = api_sgco_departamento.objects.all()
        page = self.paginate_queryset(modelo)
        if page is not None:
            serializer = self.get_paginated_response(DepartamentoSerializer(page, many=True).data)
        else:
            serializer = DepartamentoSerializer(modelo, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)


class MunicipioPaginadoAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios 
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS    

    def get(self, request, format=None):
        modelo = api_sgco_municipio.objects.all()
        page = self.paginate_queryset(modelo)
        if page is not None:
            serializer = self.get_paginated_response(MunicipioSerializer(page, many=True).data)
        else:
            serializer = MunicipioSerializer(modelo, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class MunicipioPaginadoAPIViewFilter(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios filtrados por departamento
    """

    permission_classes = [IsAuthenticated]  
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS   

    def get(self, request, pk, format=None):
        modelo = api_sgco_municipio.objects.filter(dpto=pk)
        page = self.paginate_queryset(modelo)
        if page is not None:
            serializer = self.get_paginated_response(MunicipioSerializer(page, many=True).data)
        else:
            serializer = MunicipioSerializer(modelo, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class DepartamentoAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los departamentos
    """

    permission_classes = [IsAuthenticated]  

    def get(self, request, format=None, *args, **kwargs):
        modelo = api_sgco_departamento.objects.all()
        serializer = DepartamentoSerializer(modelo, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)


class MunicipioAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios 
    """

    permission_classes = [IsAuthenticated]  

    def get(self, request, format=None):
        modelo = api_sgco_municipio.objects.all()
        
        serializer = MunicipioSerializer(modelo, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class MunicipioAPIViewFilter(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios filtrados por departamento
    """

    permission_classes = [IsAuthenticated]
    

    def get(self, request, pk, format=None):
        modelo = api_sgco_municipio.objects.filter(dpto=pk)
        serializer = MunicipioSerializer(modelo, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class MunicipioAPIViewDetail(APIView):
    """
        Funciones para:
        - Consultar recurso unico 
        - Actualizar/Editar recurso del modelo de muninipio
    """
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return api_sgco_municipio.objects.get(pk=pk)
        except api_sgco_municipio.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        municipio = self.get_object(pk)
        serializer = MunicipioSerializer(municipio)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        municipio = self.get_object(pk)
        serializer = MunicipioSerializer(municipio, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    # def delete(self, request, pk, format=None):
    #     municipio = self.get_object(pk)
    #     municipio.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)

class DepartamentoAPIViewDetail(APIView):
    """
        Funciones para:
        - Consultar recurso unico 
        - Actualizar/Editar recurso del modelo de departamento
    """
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return api_sgco_departamento.objects.get(pk=pk)
        except api_sgco_departamento.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        departamento = self.get_object(pk)
        serializer = DepartamentoSerializer(departamento)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        departamento = self.get_object(pk)
        serializer = DepartamentoSerializer(departamento, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    # def delete(self, request, pk, format=None):
    #     departamento = self.get_object(pk)
    #     departamento.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)