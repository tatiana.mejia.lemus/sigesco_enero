from django.http import JsonResponse
from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status
from apps.dane.dominios_api.serializers import DominioSerializer, ListaSerializer
from apps.dane.models.dominios import api_sgco_dominios
from apps.dane.models.listas import api_sgco_listas
from apps.dane.pagination import PaginationHandlerMixin

class DominioAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los dominios
    """

    permission_classes = [IsAuthenticated]    

    def get(self, request, format=None, *args, **kwargs):
        persona = api_sgco_dominios.objects.all()
        serializer = DominioSerializer(persona, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class DominioPaginadoAPIView(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los dominios paginados
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS    

    def get(self, request, format=None, *args, **kwargs):
        persona = api_sgco_dominios.objects.all()
        page = self.paginate_queryset(persona)
        if page is not None:
            serializer = self.get_paginated_response(DominioSerializer(page, many=True).data)
        else:
            serializer = DominioSerializer(persona, many=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)

class ListaAPIViewFilter(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios filtrados por departamento
    """

    permission_classes = [IsAuthenticated]  

    def get(self, request, pk, format=None):
        persona = api_sgco_listas.objects.filter(domi=pk)
        serializer = ListaSerializer(persona, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class ListaPaginadoAPIViewFilter(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los municipios filtrados por departamento paginados
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS    

    def get(self, request, pk, format=None):
        persona = api_sgco_listas.objects.filter(domi=pk)
        page = self.paginate_queryset(persona)
        if page is not None:
            serializer = self.get_paginated_response(ListaSerializer(page, many=True).data)
        else:
            serializer = ListaSerializer(persona, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)




    