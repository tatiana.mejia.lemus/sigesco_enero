from rest_framework import serializers
from apps.dane.models.listas import api_sgco_listas
from apps.dane.models.dominios import api_sgco_dominios

class DominioSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = api_sgco_dominios
        exclude = ['created', 'modified']


class ListaSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = api_sgco_listas
        exclude = ['created', 'modified']