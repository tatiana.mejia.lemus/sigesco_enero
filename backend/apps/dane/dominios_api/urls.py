from django.urls import path
from .views import *

app_name = 'dominios-api'

urlpatterns = [
    # URL para consultar todos los dominios
    path('dominio', DominioAPIView.as_view()),
     # URL para consultar las listas filtradas por tipo
    path('lista/<int:pk>', ListaAPIViewFilter.as_view()),
    # URL para consultar todos los dominios (paginado)
    path('dominio-page', DominioPaginadoAPIView.as_view()),
     # URL para consultar las listas filtradas por tipo (paginado)
    path('lista-page/<int:pk>', ListaPaginadoAPIViewFilter.as_view()),

]