from django.http import JsonResponse
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status
from sigesco.custom_response import CustomResponse
from apps.dane.persona_api.serializers import *
from apps.dane.models.persona import api_sgco_persona
from apps.dane.pagination import PaginationHandlerMixin
import os

class ExportAPIView(APIView, PaginationHandlerMixin):
    """
        Funciones para:
        - Generar export de información de un municipio seleccionado
    """

    def get(self, request, format=None, *args, **kwargs):
        
        mpio = self.request.query_params.get('mpio', None)
        
        if mpio is not None:
            os.system("""python manage.py dump_object dane.api_sgco_estr --query '{"mpio": %s}' > export/estratificacion.json""" %mpio)
            os.system("""python manage.py dump_object dane.api_sgco_estr_predom_zona --query '{"mpio": %s}' > export/estrato_predom_zona.json""" %mpio)
            os.system("""python manage.py dump_object dane.api_sgco_uaf --query '{"mpio": %s}' > export/uaf.json""" %mpio)
            os.system("""python manage.py dump_object dane.api_sgco_zhg --query '{"mpio": %s}' > export/zhg.json""" %mpio)

            return Response({'message' : 'Los archivos se han generado en la carpeta export'}, status=status.HTTP_201_CREATED)
        else:
            return Response({'error': 'El municipio no existe'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        