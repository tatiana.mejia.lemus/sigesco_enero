from django.urls import path
from .views import *

app_name = 'export-api'

urlpatterns = [
    # URL para exportar la información de un municipio determinado
    path('export', ExportAPIView.as_view()),

]