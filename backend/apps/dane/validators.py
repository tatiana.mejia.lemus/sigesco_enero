from django.core.exceptions import ValidationError

"""
    Validación del tamaño del archivo 
"""
def validate_file_size(value):
    limit_mb = 10
    try:
        filesize = value.size
        
        if filesize >  limit_mb * 1024 * 1024:
            raise ValidationError("El tamaño máximo de la foto debe ser de 10 MB")
        else:
            return value
    except:
        pass

"""
    Validación del tamaño de una imagen 
"""
def validate_image_size(value):
    limit_mb = 5
    try:
        filesize = value.size
        
        if filesize > limit_mb * 1024 * 1024:
            raise ValidationError("El tamaño máximo de la foto debe ser de 5 MB")
        else:
            return value
    except:
        pass

"""
    Validación de la extension de las imagenes
"""
def validate_extension_images(value):
    if (not value.name.endswith('.png') and
        not value.name.endswith('.jpeg') and 
        not value.name.endswith('.gif') and
        not value.name.endswith('.bmp') and 
        not value.name.endswith('.jpg')):
 
        raise ValidationError("Extensiones permitidas: .jpg, .jpeg, .png, .gif, .bmp")

"""
    Validación de la extensión de archivos de texto
"""
def validate_extension_text(value):
    if (not value.name.endswith('.txt')):
        raise ValidationError("Extensiones permitidas: .txt")

"""
    Validación de la extensión de archivos de tipo documento
    Las extensiones aceptadas para tipo documentos son: .pdf, .doc, .docx, .xls, .xlsx
"""
def validate_extension_files(value):
    if (not value.name.endswith('.pdf') and
        not value.name.endswith('.doc') and 
        not value.name.endswith('.docx') and
        not value.name.endswith('.xls') and 
        not value.name.endswith('.xlsx')):
        raise ValidationError("Extensiones permitidas: .pdf, .doc, .docx, .xls, .xlsx")

"""
    Validación de la extensión para archivos comprimidos
    Las extensiones aceptadas son: .zip .rar
"""
def validate_extension_requerimientos_anexos(value):
    if (not value.name.endswith('.pdf') and
        not value.name.endswith('.doc') and 
        not value.name.endswith('.docx') and
        not value.name.endswith('.xls') and 
        not value.name.endswith('.xlsx') and
        not value.name.endswith('.zip') and
        not value.name.endswith('.rar')):
        raise ValidationError("Extensiones permitidas: .pdf, .doc, .docx, .xls, .xlsx, .zip, .rar")