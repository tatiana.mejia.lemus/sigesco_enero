from django.urls import path
from .views import *

app_name = 'zhg-api'

urlpatterns = [

    # URL para listar todas las ZHG de los municipios
    path('zhg', ZhgAPIView.as_view()),
    # URL que consulta y edita un ZHG de un municipio
    path('zhg/<int:pk>', ZhgAPIViewDetail.as_view()),
    # URL que consulta las ZHG de un municipio
    path('zhg/municipio/<int:pk>', ZhgAPIViewFilter.as_view()),

]