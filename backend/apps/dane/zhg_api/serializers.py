from rest_framework import serializers
from apps.dane.models.zhg import *
from apps.dane.validators import *
from apps.dane.municipio_api.serializers import MunicipioSerializer

class ZhgSerializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_zhg
        exclude = ['created', 'modified']

class ZhgGetSerializer(serializers.ModelSerializer):
    
    mpio = MunicipioSerializer()

    class Meta:
        model = api_sgco_zhg
        exclude = ['created', 'modified']

class ZhgRequestSerializer(serializers.Serializer):
    mpio = serializers.IntegerField()
    file = serializers.FileField(allow_empty_file=False, validators=[validate_extension_text])

