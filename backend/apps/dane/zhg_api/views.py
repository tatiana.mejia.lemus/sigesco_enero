from django.http import JsonResponse
from django.http import Http404
from django.conf import settings
from django.db.models import Q
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.settings import api_settings
from sigesco.custom_response import CustomResponse
from apps.dane.zhg_api.serializers import *
from apps.dane.models.zhg import *
from apps.dane.models.estratificacion_archivos import *
from apps.dane.pagination import PaginationHandlerMixin
from datetime import date
import pandas as pd
import os

class ZhgAPIView(APIView, PaginationHandlerMixin):
    """
        Funciones para:
        - Consultar todos los datos de la ZHG registradas
        - Guardar datos en el modelo ZHG, despúes del proceso de revisión del archivo enviado
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def get(self, request, format=None, *args, **kwargs):
        fecha_desde = self.request.query_params.get('fecha_desde', None)
        fecha_hasta = self.request.query_params.get('fecha_hasta', None)
        municipio = self.request.query_params.get('municipio', None)

        if fecha_desde == '' and fecha_hasta == '' and municipio == '':
            return Response({'error': 'Todos los parametros del request están vacios'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)  
        else:
            if fecha_desde is not None and fecha_hasta is not None and municipio is not None:
                try:
                    zhg = api_sgco_zhg.objects.filter(mpio=request.GET.get('municipio'), zhg_dfcha_rgstro_zhg__range=(request.GET.get('fecha_desde'),request.GET.get('fecha_hasta')))                
                except api_sgco_zhg.DoesNotExist:
                    raise Http404
            elif fecha_desde is not None and fecha_hasta is not None:
                try:
                    zhg = api_sgco_zhg.objects.filter(zhg_dfcha_rgstro_zhg__range=(request.GET.get('fecha_desde'),request.GET.get('fecha_hasta')))                
                except api_sgco_zhg.DoesNotExist:
                    raise Http404
            elif municipio is not None:
                try:
                    zhg = api_sgco_zhg.objects.filter(mpio=request.GET.get('municipio'))
                except api_sgco_zhg.DoesNotExist:
                    raise Http404
            else:
                zhg = api_sgco_zhg.objects.all()

        page = self.paginate_queryset(zhg)
        if page is not None:
            serializer = self.get_paginated_response(ZhgGetSerializer(page, many=True).data)
        else:
            serializer = ZhgGetSerializer(zhg, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializerPost = ZhgRequestSerializer(data=request.data)
        
        if serializerPost.is_valid():  
            # Extraer municipio de la petición
            mpio = request.data['mpio']          
            # Consultar objetos para guardar el archivo en la tabla respectiva
            try:
                tiar = api_sgco_listas.objects.get(Q(domi=26), Q(lsta_iiditem=1))
            except api_sgco_listas.DoesNotExist:
                print("No se encontró el tiar buscado")
            
            try:
                espr = api_sgco_listas.objects.get(Q(domi=27), Q(lsta_iiditem=1))
            except api_sgco_listas.DoesNotExist:
                print("No se encontró el espr buscado")

            try:
                municipio = api_sgco_municipio.objects.get(id=mpio)
            except api_sgco_municipio.DoesNotExist:
                print("No se encontró el municipio buscado")

            # Hallar fecha actual 
            hoy = date.today()
            fecha = hoy.strftime("%Y-%m-%d") 
            
            # Comprobar municipio y fecha, para no permitir el guardado de información el mismo día
            zhg = api_sgco_zhg.objects.filter(Q(mpio=request.data['mpio']), Q(zhg_dfcha_rgstro_zhg=fecha)).count()
            if zhg < 1:
                # Subir archivo al servidor
                file = api_sgco_archivos_proceso_estr.objects.create(
                    arpr_farchvo_prcso = request.FILES['file'],
                    tiar=tiar,
                    arpr_dfcha_vgncia=None,
                    espr=espr,
                    mpio=municipio,
                )
                # Leer el archivo con pandas
                df = pd.read_csv(file.arpr_farchvo_prcso, sep='\t')
                # Revisar cantidad de columnas del archivo
                if(df.columns.size != 3):
                    return Response({'error': 'La cantidad de columnas de los archivos están erradas'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                # Revisar los titulos del archivo 
                if( df.columns[0] != 'ZHG' and 
                    df.columns[1] != 'AVALUO' and
                    df.columns[2] != 'AREA'):
                    return Response({'error': 'Los titulos de las columnas son erradas'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                # Revisar la cantidad de columnas del archivo
                if (df.columns.size != 3):
                    return Response({'error': 'La cantidad de columnas de los archivos está errada'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                # Recorrer el archivo y guardar datos en la tabla
                for index, row in df.iterrows():
                    data = {
                        "zhg_icdgo": row['ZHG'],
                        "zhg_navluo_hctrea": row['AVALUO'],
                        "zhg_narea": row['AREA'],
                        "zhg_navluo_ttal_zna": row['AVALUO']*row['AREA'],
                        "zhg_dfcha_rgstro_zhg": fecha,
                        "mpio": request.data['mpio']
                    }
                    serializer = ZhgSerializer(data=data)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        # Imprimir errores por consola
                        print(serializer.errors)
            else:
                try:
                    try:
                        espr = api_sgco_listas.objects.get(Q(domi=27), Q(lsta_iiditem=0))
                    except api_sgco_listas.DoesNotExist:
                        print("No se encontró el estado de archivo buscado")   

                    archivo_proceso = api_sgco_archivos_proceso_estr.objects.get(Q(mpio=request.data['mpio']), Q(tiar=758), Q(espr=762))
                    archivo_proceso.espr = espr
                    archivo_proceso.save()
                except api_sgco_archivos_proceso_estr.DoesNotExist:
                    print("Error al consultar el archivo de proceso")
                return Response({'error': 'El municipio ya subió información de zonas homogeneas en esta fecha'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            return Response(CustomResponse.HTTP_201_CREATED(), status=status.HTTP_201_CREATED)
        return Response(serializerPost.errors, status=status.HTTP_400_BAD_REQUEST)    

class ZhgAPIViewDetail(APIView):
    """
        Funciones para:
        - Consultar recurso unico 
        - Actualizar/Editar recurso del modelo ZHG
    """
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return api_sgco_zhg.objects.get(pk=pk)
        except api_sgco_zhg.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        zhg = self.get_object(pk)
        serializer = ZhgGetSerializer(zhg)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        zhg = self.get_object(pk)
        serializer = ZhgSerializer(zhg, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk, format=None):
    #     zhg = self.get_object(pk)
    #     zhg.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)

class ZhgAPIViewFilter(APIView, PaginationHandlerMixin):
    """
    Función que devuelve todos los ZHG filtrados por municipio
    """

    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS   

    def get(self, request, pk, format=None):
        persona = api_sgco_zhg.objects.filter(mpio=pk)
        page = self.paginate_queryset(persona)
        if page is not None:
            serializer = self.get_paginated_response(ZhgGetSerializer(page, many=True).data)
        else:
            serializer = ZhgGetSerializer(persona, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)