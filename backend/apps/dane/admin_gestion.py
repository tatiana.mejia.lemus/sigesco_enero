from django.contrib.admin import AdminSite
from django.contrib import admin

from apps.dane.models.entidad import api_sgco_entidad_remitente, api_sgco_estr_entidad
from apps.dane.models.gestion_requerimientos import api_sgco_estr_requerimientos, api_sgco_estr_requerimientos_anexos, api_sgco_estr_requerimientos_temas, api_sgco_estr_requerimientos_user
from apps.dane.models.listas import api_sgco_listas
class GestionAdminSite(AdminSite):
    site_header     = "SIGESCO Gestión de Requerimientos"
    site_title      = "SIGESCO Gestión de Requerimientos | Portal"
    index_title     = "Bienvenido al portal de gestion de requerimientos - SIGESCO"

gestion_admin_site = GestionAdminSite(name='gestion_admin')

class api_sgco_estr_entidad_admin(admin.ModelAdmin):
    list_display = ('enti_cnmbre', 'mpio', 'created')
    list_filter = ('enti_cnmbre', 'mpio')

gestion_admin_site.register(api_sgco_estr_entidad, api_sgco_estr_entidad_admin)

class api_sgco_entidad_remitente_admin(admin.ModelAdmin):
    list_display = ('usen_cnmbre', 'crgo', 'created')
    list_filter = ('usen_cnmbre', 'crgo')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "crgo":
            kwargs["queryset"] = api_sgco_listas.objects.filter(domi=24)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

gestion_admin_site.register(api_sgco_entidad_remitente, api_sgco_entidad_remitente_admin)

class api_sgco_estr_requerimientos_admin(admin.ModelAdmin):
    fieldsets = [
        (None,                      {'fields': ['user','enti','req_crdcdo','req_dfcha_ofcio','req_dfcha_rdcdo','req_dfcha_asgncion','req_casnto','req_cobsrvciones','req_fdcmnto_rdcdo']}),
        ('Gestión de respuestas',   {'fields': ['req_dfcha_rspuesta', 'req_cnmro_rspuesta', 'req_fdcmnto_rspuesta']}),
    ]

    list_display = ('user', 'enti', 'req_crdcdo','created')
    list_filter = ('req_crdcdo','req_dfcha_rdcdo','enti__enti_cnmbre','req_cnmro_rspuesta','created')

gestion_admin_site.register(api_sgco_estr_requerimientos, api_sgco_estr_requerimientos_admin)

class api_sgco_estr_requerimientos_anexos_admin(admin.ModelAdmin):
    
    list_display = ('req', 'rean_fanxos','created')
    list_filter = ('req__req_crdcdo','req__req_dfcha_rdcdo','req__enti__enti_cnmbre','created')

gestion_admin_site.register(api_sgco_estr_requerimientos_anexos, api_sgco_estr_requerimientos_anexos_admin)

class api_sgco_estr_requerimientos_temas_admin(admin.ModelAdmin):
    list_display = ('req', 'tema','created')
    list_filter = ('req__req_crdcdo','req__req_dfcha_rdcdo','req__enti__enti_cnmbre','created')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "tema":
            kwargs["queryset"] = api_sgco_listas.objects.filter(domi=23)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

gestion_admin_site.register(api_sgco_estr_requerimientos_temas, api_sgco_estr_requerimientos_temas_admin)

class api_sgco_estr_requerimientos_user_admin(admin.ModelAdmin):
    list_display = ('req', 'user','created')
    list_filter = ('req__req_crdcdo','req__req_dfcha_rdcdo','req__enti__enti_cnmbre','created')

gestion_admin_site.register(api_sgco_estr_requerimientos_user, api_sgco_estr_requerimientos_user_admin)
