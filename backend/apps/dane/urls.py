from django.urls import path, include

app_name = 'dane'

urlpatterns = [

    # API de Personas
    path('personas/', include('apps.dane.persona_api.urls')),
    # API de Usuarios
    path('usuarios/', include('apps.dane.user_api.urls')),
    # API de Estratificación
    path('estratificaciones/', include('apps.dane.estratificacion_api.urls')),
    # API de UAF
    path('uaf/', include('apps.dane.uaf_api.urls')), 
    # API de ZHG
    path('zhg/', include('apps.dane.zhg_api.urls')),
    # API de ZHG
    path('export/', include('apps.dane.export_api.urls')),
    
    # APIs Generales
    
    # API de municipios y departamentos
    path('ubicaciones/', include('apps.dane.municipio_api.urls')),
    # API de dominios y listas
    path('dominios/', include('apps.dane.dominios_api.urls')),

]
