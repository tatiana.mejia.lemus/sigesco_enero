from django.http import JsonResponse
from django.http import Http404
from django.conf import settings
from django.db.models import Q
from django.db.models import Avg, Count, Min, Sum
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.settings import api_settings
from sigesco.custom_response import CustomResponse
from apps.dane.pagination import PaginationHandlerMixin
from apps.dane.models.estratificacion import *
from apps.dane.models.municipio import *
from apps.dane.models.registro1_igac import *
from apps.dane.models.registro2_igac import *
from apps.dane.estratificacion_api.serializers import *
from apps.dane.municipio_api.serializers import DepartamentoSerializer, MunicipioSerializer
from apps.dane.dominios_api.serializers import DominioSerializer, ListaSerializer
from apps.dane.persona_api.serializers import PersonaSerializer
from apps.dane.models.dominios import api_sgco_dominios
from apps.dane.models.listas import api_sgco_listas
from apps.dane.models.persona import api_sgco_persona
from apps.dane.models.estratificacion_archivos import *
from apps.dane.estratificacion_api.exceptions import *
from datetime import date
from django_pandas.io import read_frame
import pandas as pd
import numpy as np
import os

class ParametrizarEstratificacionAPIView(APIView, PaginationHandlerMixin):
    """
        Función para realizar los procesos de:
            - Captura de la información catastral
                - Clasificación de predios: Excluidos o estratificables
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def post(self, request, format=None):
        parametrizarSerializer = ParametrizarEstratificacionSerializer(data=request.data)
        if parametrizarSerializer.is_valid():
            # Extraer municipio de la petición
            mpio = request.data['mpio']
            try:
                tiar1 = api_sgco_listas.objects.get(Q(domi=26), Q(lsta_iiditem=2))
            except api_sgco_listas.DoesNotExist:
                print("No se encontró el tiar buscado")
            
            try:
                tiar2 = api_sgco_listas.objects.get(Q(domi=26), Q(lsta_iiditem=3))
            except api_sgco_listas.DoesNotExist:
                print("No se encontró el tiar buscado")
            
            try:
                espr = api_sgco_listas.objects.get(Q(domi=27), Q(lsta_iiditem=1))
            except api_sgco_listas.DoesNotExist:
                print("No se encontró el espr buscado")

            try:
                municipio = api_sgco_municipio.objects.get(id=mpio)
            except api_sgco_municipio.DoesNotExist:
                print("No se encontró el municipio buscado")
            
            # Subir archivo 1 al servidor
            file1 = api_sgco_archivos_proceso_estr.objects.create(
                arpr_farchvo_prcso = request.FILES['file1'],
                tiar=tiar1,
                arpr_dfcha_vgncia=None,
                espr=espr,
                mpio=municipio,
            )
            # Subir archivo 2 al servidor
            file2 = api_sgco_archivos_proceso_estr.objects.create(
                arpr_farchvo_prcso = request.FILES['file2'],
                tiar=tiar2,
                arpr_dfcha_vgncia=None,
                espr=espr,
                mpio=municipio,
            )
            try:
                # Definir tipos de variable para el archivo IGAC1
                dtype_file1 = {
                    'DEPARTAMENTO': 'string',
                    'MUNICIPIO': 'string',
                    'NUMERO_DEL_PREDIO' : 'string',
                    'TIPO_DE_REGISTRO' : pd.Int64Dtype(),
                    'NUMERO_DE_ORDEN' : pd.Int64Dtype(),
                    'TOTAL_REGISTROS' : pd.Int64Dtype(),
                    'NOMBRE' : 'string',
                    'ESTADO_CIVIL' : 'string',
                    'TIPO_DOCUMENTO' : 'string',
                    'NUMERO_DOCUMENTO' : 'string',
                    'DIRECCION' : 'string',
                    'COMUNA' : 'string',
                    'DESTINO_ECONOMICO' : 'string',
                    'AREA_TERRENO' : 'float',
                    'AREA_CONSTRUIDA' : 'float',
                    'AVALUO' : 'float',
                    'VIGENCIA' : 'string',
                    'NUMERO_PREDIAL_NACIONAL' : 'string',
                }
                # Leer archivo df1| IGAC1
                df1 = pd.read_csv(file1.arpr_farchvo_prcso, sep='\t',dtype=dtype_file1, keep_default_na=False)
            except:
                return Response({'error': 'Error al leer archivo IGAC1'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            try:
                # Definir tipos de variable para el archivo IGAC2
                dtype_file2 = {
                    'DEPARTAMENTO' : 'string',
                    'MUNICIPIO' : 'string',
                    'NUMERO_DEL_PREDIO' : 'string',
                    'TIPO_DE_REGISTRO' : pd.Int64Dtype(),
                    'NUMERO_DE_ORDEN' : pd.Int64Dtype(),
                    'TOTAL_REGISTROS' : pd.Int64Dtype(),
                    'ZONA_ECONOMICA_1' : pd.Int64Dtype(),
                    'AREA_TERRENO_1' : 'float',
                    'ZONA_ECONOMICA_2' : pd.Int64Dtype(),
                    'AREA_TERRENO_2' : 'float',
                    'HABITACIONES_1' : pd.Int64Dtype(),
                    'USO_1' : pd.Int64Dtype(),
                    'PUNTAJE_1' : pd.Int64Dtype(),
                    'AREA_CONSTRUIDA_1' : 'float',
                    'HABITACIONES_2' : pd.Int64Dtype(),
                    'USO_2' : pd.Int64Dtype(),
                    'PUNTAJE_2' : pd.Int64Dtype(),
                    'AREA_CONSTRUIDA_2' : 'float',
                    'HABITACIONES_3' : pd.Int64Dtype(),
                    'USO_3' : pd.Int64Dtype(),
                    'PUNTAJE_3' : pd.Int64Dtype(),
                    'AREA_CONSTRUIDA_3' : 'float',
                    'VIGENCIA' : 'string',
                }
                # Leer archivo df2| IGAC2
                df2 = pd.read_csv(file2.arpr_farchvo_prcso, sep='\t',dtype=dtype_file2)
            except :
                return Response({'error': 'Error al leer archivo IGAC2'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            try:
                # Eliminar filas duplicadas y guardar la primera fila en el archivo IGAC1
                df1 = df1.drop_duplicates(subset='NUMERO_DEL_PREDIO', keep='first')
            except:
                return Response({'error': 'Compruebe el archivo. Error al intentar eliminar duplicados en el archivo IGAC1'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            # Consultar codigo del municipio con el id enviado por request            
            try:
                query = api_sgco_municipio.objects.get(pk=mpio)
            except api_sgco_municipio.DoesNotExist:
                raise MunicipioNoEncontradoRequest()
            serializer = MunicipioSerializer(query)
            mpio_ccdgo = serializer.data['mpio_ccdgo']
            """ 
                Validación de numero de columnas en cada archivo 
                - Para IGAC 1 = 18 columnas 
                - Para IGAC 2 = 39 columnas
            """
            if (df1.columns.size == 18 and df2.columns.size == 39):
                # Valida si todos los municipios son iguales en cada archivo 
                if (all(df1['MUNICIPIO']) and all(df2['MUNICIPIO'])):
                    # Comprueba si el mpio enviado por el request es igual al municipio de los archivos
                    if(mpio_ccdgo[2:5] == df1['MUNICIPIO'][0] and mpio_ccdgo[2:5] == df2['MUNICIPIO'][0]):
                        # Comprueba que las vigencias de los dos archivos sean iguales
                        if(all(df1['VIGENCIA']) and all(df2['VIGENCIA'])):
                            # Comprobar si el archivo es para nuevo/actualización o renovación
                            igac1 = api_sgco_estr_rgstro1_igac.objects.filter(Q(mpio=mpio), Q(is_removed=False)).count()
                            if igac1 > 0:
                                vigencia_guardada = api_sgco_estr_rgstro1_igac.objects.filter(Q(mpio=mpio), Q(is_removed=False)).first()
                                if df1['VIGENCIA'][0] == vigencia_guardada.reg1_cvgncia:
                                    print("Actualizacion del proceso de estratificación")
                                else:
                                    print("Renovación del proceso de estratificación") 
                                    igac1 = api_sgco_estr_rgstro1_igac.objects.filter(Q(mpio=mpio), Q(is_removed=False))
                                    igac1.delete()

                                    igac2 = api_sgco_estr_rgstro2_igac.objects.filter(Q(mpio=mpio), Q(is_removed=False))
                                    igac2.delete()
                            else:
                                print("Nuevo proceso de estratificación")

                            # Captura de información para IGAC1
                            self.capturar_informacion_igac1(df1, mpio_ccdgo)
                            # Captura de información para IGAC2
                            self.capturar_informacion_igac2(df2, mpio_ccdgo) 
                            # Eliminar información de estratificaciones anteriores de la misma vigencia
                            vigencia = df1['VIGENCIA'][0]
                            estr = api_sgco_estr.objects.filter(Q(mpio=mpio), Q(estr_cvgncia=vigencia))
                            estr.delete()
                            # Clasificar predios
                            self.clasificar_predios(mpio, vigencia)
                            return Response(CustomResponse.HTTP_201_CREATED(), status=status.HTTP_201_CREATED)                           
                        else:
                            return Response({'error': 'Las vigencias en los archivos son diferentes'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) 
                    else: 
                        return Response({'error': 'El muninipio enviado no corresponde al municipio de los archivos'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) 
                else:
                    return Response({'error': 'No todos los municipios en los archivos son iguales'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) 
            else:
                return Response({'error': 'La cantidad de columnas de los archivos está errada'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)              
        else:
            return Response(parametrizarSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def capturar_informacion_igac1(self, df1, mpio_ccdgo):
        """
            Archivo IGAC 1
        """
        # Destino Economico
        query = api_sgco_listas.objects.filter(domi=21)
        df_deec = read_frame(query)                        
        serie_deec = pd.Series(df_deec['id'])
        serie_deec.index = df_deec['lsta_cabrviatra'] 
        df1['DESTINO_ECONOMICO'] = df1['DESTINO_ECONOMICO'].map(serie_deec)
        # Consultar municipio
        df1['mpio_ccdgo'] = df1['DEPARTAMENTO'] + df1['MUNICIPIO']
        query = api_sgco_municipio.objects.all()
        df_mpio = read_frame(query)                        
        serie_mpio = pd.Series(df_mpio['id'])
        serie_mpio.index = df_mpio['mpio_ccdgo'] 
        df1['mpio_ccdgo'] = df1['mpio_ccdgo'].map(serie_mpio)
        # Consultar tipo de documento
        query = api_sgco_listas.objects.filter(domi=1)
        df_tipDoc = read_frame(query)
        index_tipDoc = {
            'CC':'C',
            'TI': 'T',
            'NIT': 'N',
            'CE': 'E',
            'PA': 'P',
            'NUIP': 'U',
            'RC': 'R',
            'X': 'X'
        }
        df_tipDoc['lsta_cabrviatra'] = df_tipDoc['lsta_cabrviatra'].map(index_tipDoc)              
        serie_tipDoc = pd.Series(df_tipDoc['id'])
        serie_tipDoc.index = df_tipDoc['lsta_cabrviatra'] 
        df1['TIPO_DOCUMENTO'] = df1['TIPO_DOCUMENTO'].map(serie_tipDoc)
        # Consultar comuna 
        serie_icmna = {
            '': None,
        }
        df1['COMUNA'] = df1['COMUNA'].map(serie_icmna)
        # Consultar estado civil
        query = api_sgco_listas.objects.filter(Q(domi=2), Q(lsta_iiditem__in=['7', '8', '9']))
        df_esci = read_frame(query)                        
        serie_esci = pd.Series(df_esci['id'])
        serie_esci.index = df_esci['lsta_cabrviatra']
        df1['ESTADO_CIVIL'] = df1['ESTADO_CIVIL'].map(serie_esci)        
        # Consultar tipo de registro catastral
        query = api_sgco_listas.objects.filter(domi=20)
        df_treg = read_frame(query)                        
        serie_treg = pd.Series(df_treg['id'])
        serie_treg.index = df_treg['lsta_iiditem'] 
        df1['TIPO_DE_REGISTRO'] = df1['TIPO_DE_REGISTRO'].map(serie_treg)                 
        # Renombrar columnas del df1
        columns_df1 = {
            # Modelo IGAC1
            'NUMERO_DEL_PREDIO': 'reg1_cnmro_prdial',
            'NUMERO_DE_ORDEN': 'reg1_inmro_orden',
            'TOTAL_REGISTROS': 'reg1_ittal_rgstros',
            'DIRECCION': 'reg1_cdrccion',
            'COMUNA': 'reg1_icmna',
            'DESTINO_ECONOMICO': 'deec',
            'AREA_TERRENO': 'reg1_narea_trrno',
            'AREA_CONSTRUIDA': 'reg1_narea_cnstruida',
            'AVALUO': 'reg1_navluo',
            'VIGENCIA': 'reg1_cvgncia',
            'NUMERO_PREDIAL_NACIONAL': 'reg1_inmro_prdial_ncional',
            'TIPO_DE_REGISTRO': 'treg',
            'mpio_ccdgo': 'mpio',
            # Modelo persona
            'NOMBRE': 'pers_cnmbre',
            'NUMERO_DOCUMENTO': 'pers_nidntfccion',
            'TIPO_DOCUMENTO': 'tiid',
            'ESTADO_CIVIL': 'esci'
        }
        # Renombrar columnas como parametros del modelo
        df1 = df1.rename(columns=columns_df1)
        # Seleccionar parametros necesarios para enviar
        df1 = df1[[
            'reg1_cnmro_prdial', 
            'reg1_inmro_orden', 
            'reg1_ittal_rgstros', 
            'reg1_cdrccion', 
            'reg1_icmna', 
            'deec', 
            'reg1_narea_trrno', 
            'reg1_narea_cnstruida', 
            'reg1_navluo', 
            'reg1_cvgncia', 
            'reg1_inmro_prdial_ncional', 
            'treg', 
            'pers_cnmbre', 
            'pers_nidntfccion', 
            'tiid', 
            'esci', 
            'mpio'
        ]]
        # Convertir espacios nulos en None (formato en Python)
        df1 = df1.where((pd.notnull(df1)), None)
        # Convertir df1 en diccionario Python
        data = df1.to_dict('records')
        # Enviar datos del dataframe al serializador para luego guardarlos
        registro1IgacArchivoSerializer = Registro1IgacArchivoSerializer(data=data, many=True)
        if registro1IgacArchivoSerializer.is_valid():
            registro1IgacArchivoSerializer.save()
        else:
            # Imprimir errores por consola
            print(registro1IgacArchivoSerializer.errors)
            return Response({'error': 'Se presentó error al cargar el archivo de IGAC1'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    def capturar_informacion_igac2(self, df2, mpio_ccdgo):
        """
            Archivo IGAC 2
        """        
        # Consultar municipio
        df2['mpio_ccdgo'] = df2['DEPARTAMENTO'] + df2['MUNICIPIO']
        query = api_sgco_municipio.objects.all()
        df_mpio = read_frame(query)                        
        serie_mpio = pd.Series(df_mpio['id'])
        serie_mpio.index = df_mpio['mpio_ccdgo'] 
        df2['mpio_ccdgo'] = df2['mpio_ccdgo'].map(serie_mpio)
        # Consultar tipo de registro catastral
        query = api_sgco_listas.objects.filter(domi=20)
        df_treg = read_frame(query)                        
        serie_treg = pd.Series(df_treg['id'])
        serie_treg.index = df_treg['lsta_iiditem'] 
        df2['TIPO_DE_REGISTRO'] = df2['TIPO_DE_REGISTRO'].map(serie_treg) 
        # Consultar usos
        query = api_sgco_listas.objects.filter(domi=25)
        df_uso = read_frame(query)                        
        serie_tuso = pd.Series(df_uso['id'])
        serie_tuso.index = df_uso['lsta_iiditem'] 
        df2['USO_1'] = df2['USO_1'].map(serie_tuso)
        df2['USO_2'] = df2['USO_2'].map(serie_tuso)
        df2['USO_3'] = df2['USO_3'].map(serie_tuso)
        # Renombrar columnas del df2
        columns_df2 = {
            # Modelo IGAC2
            'mpio_ccdgo': 'mpio',
            'NUMERO_DEL_PREDIO': 'reg2_cnmro_prdial',
            'TIPO_DE_REGISTRO' : 'treg',
            'NUMERO_DE_ORDEN': 'inmro_orden',
            'TOTAL_REGISTROS': 'reg2_ittal_rgstros',
            'VIGENCIA': 'reg2_cvgncia',
            # Modelo IGAC construcciones
            'HABITACIONES_1': 'cons_ihbtciones1',
            'USO_1': 'uso1',
            'PUNTAJE_1': 'cons_ipntje1',
            'AREA_CONSTRUIDA_1': 'cons_narea_cnstruida1',
            'HABITACIONES_2': 'cons_ihbtciones2',
            'USO_2': 'uso2',
            'PUNTAJE_2': 'cons_ipntje2',
            'AREA_CONSTRUIDA_2': 'cons_narea_cnstruida2',
            'HABITACIONES_3': 'cons_ihbtciones3',
            'USO_3': 'uso3',
            'PUNTAJE_3': 'cons_ipntje3',
            'AREA_CONSTRUIDA_3': 'cons_narea_cnstruida3',
            # Modelo IGAC Zonas
            'ZONA_ECONOMICA_1': 'eszn_izna_ecnmca1',
            'AREA_TERRENO_1': 'eszn_narea_trrno1',
            'ZONA_ECONOMICA_2': 'eszn_izna_ecnmca2',
            'AREA_TERRENO_2': 'eszn_narea_trrno2',
        }
        # Renombrar columnas como parametros del modelo
        df2 = df2.rename(columns=columns_df2)
        # Seleccionar parametros necesarios para enviar
        df2 = df2[[
            'reg2_cnmro_prdial',
            'treg',
            'inmro_orden',
            'reg2_ittal_rgstros',
            'reg2_cvgncia',
            'cons_ihbtciones1',
            'uso1',
            'cons_ipntje1',
            'cons_narea_cnstruida1',
            'cons_ihbtciones2',
            'uso2',
            'cons_ipntje2',
            'cons_narea_cnstruida2',
            'cons_ihbtciones3',
            'uso3',
            'cons_ipntje3',
            'cons_narea_cnstruida3',
            'eszn_izna_ecnmca1',
            'eszn_narea_trrno1',
            'eszn_izna_ecnmca2',
            'eszn_narea_trrno2',
            'mpio'
        ]]
        # Convertir espacios nulos en None (formato en Python)
        df2 = df2.where((pd.notnull(df2)), None)
        # Convertir df2 en diccionario Python
        data = df2.to_dict('records')
        # Enviar datos del dataframe al serializador para luego guardarlos
        registro2IgacArchivoSerializer = Registro2IgacArchivoSerializer(data=data, many=True)
        if registro2IgacArchivoSerializer.is_valid():
            registro2IgacArchivoSerializer.save()
        else:
            # Imprimir errores por consola
            print(registro2IgacArchivoSerializer.errors)
            return Response({'error': 'Se presentó error al cargar el archivo de IGAC2'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def clasificar_predios(self, mpio, vigencia):
        """
            Funcion para:
            - Clasificar predios en: excluidos y estratificables

            1) Evaluar 1 zona en el predio

            2) Filtrar por criterios de exclusión:
                - Predio no rural #13
                - Registro 1 sin Registro 2 #15
                - Registro 2 sin Registro 1 #16
                - Predio sin zona homogénea geoeconómica #17
                - Predio sin área de terreno #18
                - Vivienda sin puntaje #19
                - Predio sin vivienda #20
                - Suma de áreas del Registro 2 diferente al área del Registro 1 #22
        """
        try:
            municipio = api_sgco_municipio.objects.get(id=mpio)
        except api_sgco_municipio.DoesNotExist:
            print("No se encontró el municipio buscado") 

        """
            Evaluar 1 zona en el predio | Calcular avaluo
        """
        predios_avaluo = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select *
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                left join dane_api_sgco_estr_rgstro2_igac_znas zonas on reg2.id = zonas.reg2_id
                where reg1.id in (
                    select reg1.id
                    from dane_api_sgco_estr_rgstro1_igac reg1
                    left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                    left join dane_api_sgco_estr_rgstro2_igac_znas zonas on reg2.id = zonas.reg2_id
                    where reg1.is_removed = false 
                    and reg1.mpio_id = %s
                    group by reg1.id
                    having count(zonas.eszn_inmro_orden) = 1
                    order by reg1.id
                )
                order by reg1.id''', [municipio.id])
        
        lista_predios = []
        lista_excluidos = []
        
        lista_exclusion_una_zona = []
        lista_exclusion_zonas = []
        
        # Evaluar avaluo para una zona en el predio
        for item in predios_avaluo:
            lista_predios.append(item.id)
            try:
                zona_homogenea = api_sgco_zhg.objects.get(Q(zhg_icdgo=item.eszn_izna_ecnmca), Q(mpio=municipio))
                if item.reg1_navluo == zona_homogenea.zhg_navluo_ttal_zna:
                    # ID's para validar exclusiones
                    lista_exclusion_una_zona.append(item.id)
                else:
                    # ID's para predios excluidos
                    lista_excluidos.append(item.id)
            except api_sgco_zhg.DoesNotExist:
                print("No se encontró la ZHG buscada")
        
        # Predios excluidos   
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")  

        self.predios_excluidos(municipio, lista_excluidos, exclusion, vigencia)

        if lista_exclusion_una_zona:
            lista_exclusion_una_zona = sorted(list(set(lista_exclusion_una_zona)))

            # Criterios de exclusión
            lista_predio_no_rural = self.predio_no_rural(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_no_rural
            lista_registro1_sin_registro2 = self.registro1_sin_registro2(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_registro1_sin_registro2
            lista_registro2_sin_registro1 = self.registro2_sin_registro1(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_registro2_sin_registro1
            lista_predio_sin_zhg = self.predio_sin_zhg(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_sin_zhg
            lista_predio_sin_area = self.predio_sin_area(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_sin_area
            lista_vivienda_sin_puntaje = self.vivienda_sin_puntaje(municipio, lista_exclusion_una_zona, vigencia)
            lista_excluidos = lista_excluidos + lista_vivienda_sin_puntaje
            lista_predio_sin_vivienda = self.predio_sin_vivienda(municipio, lista_exclusion_una_zona, vigencia)        
            lista_excluidos = lista_excluidos + lista_predio_sin_vivienda
            lista_suma_areas_diferentes = self.suma_areas_diferentes(municipio, lista_exclusion_una_zona, vigencia) 
            lista_excluidos = lista_excluidos + lista_suma_areas_diferentes       

        """
            Evaluar 0 o varias zona en el predio 
        """
        predios_avaluo = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                left join dane_api_sgco_estr_rgstro2_igac_znas zonas on reg2.id = zonas.reg2_id
                where reg1.is_removed = false 
                and reg1.mpio_id = %s
                group by reg1.id
                having count(zonas.eszn_inmro_orden) != 1
                order by reg1.id''', [municipio.id])
        
        # Armar lista de predios con 0 o mas de una zona
        for item in predios_avaluo:
            lista_predios.append(item.id)
            # ID's para validar exclusiones
            lista_exclusion_zonas.append(item.id)
        
        if lista_exclusion_zonas:
            lista_exclusion_zonas = sorted(list(set(lista_exclusion_zonas)))

            # Criterios de exclusión
            lista_predio_no_rural = self.predio_no_rural(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_no_rural

            lista_registro1_sin_registro2 = self.registro1_sin_registro2(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_registro1_sin_registro2

            lista_registro2_sin_registro1 = self.registro2_sin_registro1(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_registro2_sin_registro1

            lista_predio_sin_zhg = self.predio_sin_zhg(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_sin_zhg

            lista_predio_sin_area = self.predio_sin_area(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_predio_sin_area

            lista_vivienda_sin_puntaje = self.vivienda_sin_puntaje(municipio, lista_exclusion_zonas, vigencia)
            lista_excluidos = lista_excluidos + lista_vivienda_sin_puntaje

            lista_predio_sin_vivienda = self.predio_sin_vivienda(municipio, lista_exclusion_zonas, vigencia)        
            lista_excluidos = lista_excluidos + lista_predio_sin_vivienda

            lista_suma_areas_diferentes = self.suma_areas_diferentes(municipio, lista_exclusion_zonas, vigencia) 
            lista_excluidos = lista_excluidos + lista_suma_areas_diferentes


        # Predios estratificables 
        estratificables = set(lista_predios) - set(lista_excluidos)
        lista_estratificables = list(estratificables)
        if lista_estratificables:
            self.predios_estratificables(municipio, lista_estratificables, vigencia)
    
    def predio_no_rural(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=2))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id,
                reg1.reg1_cnmro_prdial as reg1_predial
                from dane_api_sgco_estr_rgstro1_igac reg1
                where reg1.is_removed = false
                and substring(reg1.reg1_cnmro_prdial from 1 for 2) != '00'
                and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)

        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios

    def registro1_sin_registro2(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=4))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
            reg1.reg1_cnmro_prdial as reg1_predial,
            reg2.reg2_cnmro_prdial as reg2_predial
            from dane_api_sgco_estr_rgstro1_igac reg1
            left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial
            where reg1.is_removed = false
            and reg2_cnmro_prdial is null
            and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)

        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios
    
    def registro2_sin_registro1(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=5))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
            reg1.reg1_cnmro_prdial as reg1_predial, 
            reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
            reg1.pers_id as reg1_persona,
            reg2.reg2_cnmro_prdial as reg2_predial
            from dane_api_sgco_estr_rgstro1_igac reg1
            right join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial
            where reg1.is_removed = false
            and reg2_cnmro_prdial is null
            and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios

    def predio_sin_zhg(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=6))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona,
                reg2.reg2_cnmro_prdial as reg2_predial
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                left join dane_api_sgco_estr_rgstro2_igac_znas zonas on reg2.id = zonas.reg2_id
                where reg1.is_removed = false 
                and reg2_cnmro_prdial is null
                and reg1.id in %s
                group by reg1.reg1_cnmro_prdial, reg1.reg1_inmro_prdial_ncional, reg2.reg2_cnmro_prdial, reg1.pers_id, reg1.id''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios
    
    def predio_sin_area(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=7))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona
                from dane_api_sgco_estr_rgstro1_igac reg1
                where reg1.is_removed = false 
                and reg1_narea_trrno = 0
                and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios
    
    def vivienda_sin_puntaje(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=8))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                left join dane_api_sgco_estr_igac_cnstrcciones daseic  on reg2.id = daseic.reg2_id
                where reg1.is_removed = false 
                and reg2_cnmro_prdial is null
                and daseic.cons_ipntje = 0
                and uso_id in (658,671,684,710,720,727,728,729,736,692,697,713)
                and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios

    def predio_sin_vivienda(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=9))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                left join dane_api_sgco_estr_igac_cnstrcciones daseic  on reg2.id = daseic.reg2_id
                where reg1.is_removed = false 
                and reg2_cnmro_prdial is null
                and uso_id in (658,671,684,710,720,727,728,729,736,692,697,713)
                and reg1.id in %s''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios
    
    def suma_areas_diferentes(self, municipio, predios, vigencia):
        excluir_predios = []
        try:
            exclusion = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=11))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")                  

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona,
                reg2.reg2_cnmro_prdial as reg2_predial,
                SUM(daseic.cons_narea_cnstruida) as sum_aconst
                from dane_api_sgco_estr_rgstro1_igac reg1
                join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial 
                join dane_api_sgco_estr_igac_cnstrcciones daseic  on reg2.id = daseic.reg2_id
                where reg1.is_removed = false 
                and reg1.id in %s
                group by reg1.reg1_cnmro_prdial, reg2.reg2_cnmro_prdial, reg1.reg1_narea_cnstruida, reg1.id, reg1.reg1_inmro_prdial_ncional, reg1.pers_id
                having SUM(daseic.cons_narea_cnstruida) != reg1.reg1_narea_cnstruida''', [tuple(predios)])
        
        for item in query:
            excluir_predios.append(item.id)
        
        if excluir_predios:
            self.predios_excluidos(municipio, excluir_predios, exclusion, vigencia)

        return excluir_predios

    def predios_excluidos(self, municipio, lista, exclusion, vigencia):
        # Hallar fecha actual 
        hoy = date.today()
        fecha = hoy.strftime("%Y-%m-%d") 
        
        try:
            metodologia = api_sgco_listas.objects.get(Q(domi=22), Q(lsta_iiditem=2))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de metolodogía buscado")

        try:
            est_formulario = api_sgco_listas.objects.get(Q(domi=28), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el estado del formulario buscado") 
        
        try:
            tipo_vivienda = api_sgco_listas.objects.get(Q(domi=6), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de vivienda buscado")

        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select distinct 
                reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg2.reg2_cnmro_prdial as reg2_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona,
                reg1.reg1_cdrccion as reg1_cdrccion,
                reg1.reg1_narea_trrno as reg1_narea_trrno,
                reg1.reg1_navluo as reg1_navluo,
                case when substring(reg1.reg1_cnmro_prdial from 1 for 2) = '00' and count(daseic.cons_inmro_orden) > 0 then True end as estr_btiene_vvienda,
                count(daseic.cons_inmro_orden) as estr_icntdad_vviendas
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial
                left join dane_api_sgco_estr_igac_cnstrcciones daseic  on reg2.id = daseic.reg2_id
                where 
                reg1.id in %s
                group by reg1.id, daseic.uso_id, reg2.reg2_cnmro_prdial 
                order by reg1.id''', [tuple(lista)])

        for item in query:
            try:        
                predio = api_sgco_estr.objects.get(estr_inmro_prdial=item.reg1_predial)
            except api_sgco_estr.DoesNotExist:
                try:
                    persona = api_sgco_persona.objects.get(id=item.reg1_persona)
                except api_sgco_persona.DoesNotExist:
                    print("No se encontró la persona buscada") 
                
                predio_excluido = api_sgco_estr.objects.create(
                    estr_crta=None,
                    estr_iorden_rcrrido=None,
                    estr_cvrda=None,
                    estr_crclctor=None,
                    estr_dfcha_rgstro=fecha,
                    estr_inmro_prdial=item.reg1_predial,
                    estr_inmro_prdial_ncional=item.reg1_inmro_prdial_ncional,
                    estr_inmro_prdial_tmpral=None,
                    estr_nzna=None,
                    estr_cdrccion_prdio=item.reg1_cdrccion,
                    estr_btiene_vvienda=item.estr_btiene_vvienda,
                    estr_icntdad_vviendas= item.estr_icntdad_vviendas,
                    estr_farea_dmstrda_mtros=item.reg1_narea_trrno,
                    estr_farea_dmstrda_hctareas=item.reg1_narea_trrno / 10000,
                    estr_fdcmntos=None,
                    estr_navluo=item.reg1_navluo,
                    estr_cdscrpcion_mtvo_vsta=None,
                    estr_iestrto=None,
                    estr_cvgncia=vigencia,
                    movi=None,
                    tiex=exclusion,
                    time=metodologia,
                    mpio=municipio,
                    pers=persona,
                    esfo=est_formulario,
                    ties=None 
                )

                igac2 = api_sgco_estr_rgstro2_igac_znas.objects.filter(reg2__reg2_cnmro_prdial=item.reg2_predial)
                for item_igac2 in igac2:
                    api_sgco_estr_area_predio.objects.create(
                        esap_izna=item_igac2.eszn_izna_ecnmca,
                        esap_nhctreas=item_igac2.eszn_narea_trrno / 10000,
                        esap_nmtros=item_igac2.eszn_narea_trrno,
                        estr=predio_excluido,
                    )

                igac_construcciones = api_sgco_estr_igac_cnstrcciones.objects.filter(reg2__reg2_cnmro_prdial=item.reg2_predial)
                for item_igac_construcciones in igac_construcciones:
                    api_sgco_estr_caracter_vivienda.objects.create(
                        ecsv_narea_vvienda=item_igac_construcciones.cons_narea_cnstruida,
                        ecsv_inmro_hbtcions=item_igac_construcciones.cons_ihbtciones,
                        escv_ipntje=item_igac_construcciones.cons_ipntje,
                        tpvi=tipo_vivienda,
                        estr=predio_excluido,
                    )
    
    def predios_estratificables(self, municipio, lista, vigencia):
         # Hallar fecha actual 
        hoy = date.today()
        fecha = hoy.strftime("%Y-%m-%d") 
        
        try:
            metodologia = api_sgco_listas.objects.get(Q(domi=22), Q(lsta_iiditem=2))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de metolodogía buscado")

        try:
            est_formulario = api_sgco_listas.objects.get(Q(domi=28), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el estado del formulario buscado")

        try:
            estratificable = api_sgco_listas.objects.get(Q(domi=3), Q(lsta_iiditem=0))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de exclusión buscado")
        
        try:
            tipo_vivienda = api_sgco_listas.objects.get(Q(domi=6), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de vivienda buscado")
        
        query = api_sgco_estr_rgstro1_igac.objects.raw(
            '''select distinct 
                reg1.id as id, 
                reg1.reg1_cnmro_prdial as reg1_predial, 
                reg2.reg2_cnmro_prdial as reg2_predial, 
                reg1.reg1_inmro_prdial_ncional as reg1_inmro_prdial_ncional,
                reg1.pers_id as reg1_persona,
                reg1.reg1_cdrccion as reg1_cdrccion,
                reg1.reg1_narea_trrno as reg1_narea_trrno,
                reg1.reg1_navluo as reg1_navluo,
                case when substring(reg1.reg1_cnmro_prdial from 1 for 2) = '00' and count(daseic.cons_inmro_orden) > 0 then True end as estr_btiene_vvienda,
                count(daseic.cons_inmro_orden) as estr_icntdad_vviendas
                from dane_api_sgco_estr_rgstro1_igac reg1
                left join dane_api_sgco_estr_rgstro2_igac reg2 on reg1.reg1_cnmro_prdial = reg2.reg2_cnmro_prdial
                left join dane_api_sgco_estr_igac_cnstrcciones daseic  on reg2.id = daseic.reg2_id
                where 
                reg1.id in %s
                group by reg1.id, daseic.uso_id, reg2.reg2_cnmro_prdial 
                order by reg1.id''', [tuple(lista)])

        for item in query:
            try:        
                predio = api_sgco_estr.objects.get(estr_inmro_prdial=item.reg1_predial)
            except api_sgco_estr.DoesNotExist:
                try:
                    persona = api_sgco_persona.objects.get(id=item.reg1_persona)
                except api_sgco_persona.DoesNotExist:
                    print("No se encontró la persona buscada") 
                
                predio_estratificable = api_sgco_estr.objects.create(
                    estr_crta=None,
                    estr_iorden_rcrrido=None,
                    estr_cvrda=None,
                    estr_crclctor=None,
                    estr_dfcha_rgstro=fecha,
                    estr_inmro_prdial=item.reg1_predial,
                    estr_inmro_prdial_ncional=item.reg1_inmro_prdial_ncional,
                    estr_inmro_prdial_tmpral=None,
                    estr_nzna=None,
                    estr_cdrccion_prdio=item.reg1_cdrccion,
                    estr_btiene_vvienda=item.estr_btiene_vvienda,
                    estr_icntdad_vviendas= item.estr_icntdad_vviendas,
                    estr_farea_dmstrda_mtros=item.reg1_narea_trrno,
                    estr_farea_dmstrda_hctareas=item.reg1_narea_trrno / 10000,
                    estr_fdcmntos=None,
                    estr_navluo=item.reg1_navluo,
                    estr_cdscrpcion_mtvo_vsta=None,
                    estr_iestrto=None,
                    estr_cvgncia=vigencia,
                    movi=None,
                    tiex=estratificable,
                    time=metodologia,
                    mpio=municipio,
                    pers=persona,
                    esfo=est_formulario,
                    ties=None
                )

                igac2 = api_sgco_estr_rgstro2_igac_znas.objects.filter(reg2__reg2_cnmro_prdial=item.reg2_predial)
                for item_igac2 in igac2:
                    api_sgco_estr_area_predio.objects.create(
                        esap_izna=item_igac2.eszn_izna_ecnmca,
                        esap_nhctreas=item_igac2.eszn_narea_trrno / 10000,
                        esap_nmtros=item_igac2.eszn_narea_trrno,
                        estr=predio_estratificable,
                    )

                igac_construcciones = api_sgco_estr_igac_cnstrcciones.objects.filter(reg2__reg2_cnmro_prdial=item.reg2_predial)
                for item_igac_construcciones in igac_construcciones:
                    api_sgco_estr_caracter_vivienda.objects.create(
                        ecsv_narea_vvienda=item_igac_construcciones.cons_narea_cnstruida,
                        ecsv_inmro_hbtcions=item_igac_construcciones.cons_ihbtciones,
                        escv_ipntje=item_igac_construcciones.cons_ipntje,
                        tpvi=tipo_vivienda,
                        estr=predio_estratificable,
                    )


class ReporteClasificacionPrediosAPIView(APIView, PaginationHandlerMixin):
    """
       Función para realizar los procesos:
            - Mostrar resumen de predios excluidos o estratificables | resumen
            - Mostrar reporte de predios excluidos | excluidos
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def get(self, request, format=None):
        reporte = self.request.query_params.get('reporte', None)
        municipio = self.request.query_params.get('municipio', None)

        if reporte == '' and municipio == '':
            return Response({'error': 'Todos los parametros del request están vacios'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR) 
        else:
            if reporte is not None and municipio is not None:
                if reporte == 'resumen':
                    predio = api_sgco_estr.objects.filter(Q(mpio=municipio))
                elif reporte == 'excluidos':
                    predio = api_sgco_estr.objects.filter(Q(mpio=municipio), Q(tiex__in=[12,13,14,15,16,17,18,19,20,21,22]))
            else:
                return Response({'error': 'El request debe incluir: reporte y municipio'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)   
        
        page = self.paginate_queryset(predio)
        if page is not None:
            serializer = self.get_paginated_response(EstratificacionSerializer(page, many=True).data)
        else:
            serializer = EstratificacionSerializer(predio, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
                

class EstratificacionInicialAPIView(APIView, PaginationHandlerMixin):
    """
        Función para realizar los procesos:
            - Almacenar calculos complementarios
            - Estratificación inicial
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def post(self, request, format=None):
        # Extraer municipio de la petición
        mpio = request.data['mpio']

        # Almacenar cálculos complementarios
        self.calculos_complementarios(mpio) 

        # Almacenar cálculos complementarios
        self.estratificacion_inicial(mpio)         

        return Response(CustomResponse.HTTP_200_OK(), status=status.HTTP_200_OK) 

    def calculos_complementarios(self, mpio):
        """
            Almacenar cálculos complementarios:
                1. Calcular Avalúo total por Zona (Realizado con anterioridad)
                2. Calcular el Valor promedio de la Ha=suma(Avalúo total por Zona)/suma(Areas por zona)
                3. Calcular Valor catastral UAF=2xUAFpm(consultar en la tabla de UAFpm)
                4. Calcular la UAFzonal=3/Avaluo por Ha
        """

        # 2. Calcular el Valor promedio de la Ha=suma(Avalúo total por Zona)/suma(Areas por zona)
        zhg_narea = api_sgco_zhg.objects.raw(
            '''select 1 as id, sum(zhg_narea) as sum
            from dane_api_sgco_zhg dasz 
            where zhg_dfcha_rgstro_zhg = (
                select max(zhg_dfcha_rgstro_zhg)
                from dane_api_sgco_zhg dasz 
                where dasz.mpio_id = %s)
            and dasz.mpio_id = %s''', [mpio, mpio])
        
        zhg_navluo_ttal_zna = api_sgco_zhg.objects.raw(
            '''select 1 as id,sum(zhg_navluo_ttal_zna) as sum
                from dane_api_sgco_zhg dasz 
                where zhg_dfcha_rgstro_zhg = (
                    select max(zhg_dfcha_rgstro_zhg)
                    from dane_api_sgco_zhg dasz 
                    where dasz.mpio_id = %s)
                and dasz.mpio_id = %s''', [mpio, mpio])

        prom_hectarea = zhg_navluo_ttal_zna[0].sum / zhg_narea[0].sum

        uaf_dfcha_aval = api_sgco_uaf.objects.raw(
            '''select id, uaf_npm
                from dane_api_sgco_uaf dasu 
                where uaf_dfcha_aval = (
                    select max(uaf_dfcha_aval)
                    from dane_api_sgco_uaf dasu
                    where dasu.mpio_id = %s
                ) and dasu.mpio_id = %s''', [mpio, mpio])
        
        #3. Calcular Valor catastral UAF=2xUAFpm(consultar en la tabla de UAFpm)
        valor_catastral = prom_hectarea * uaf_dfcha_aval[0].uaf_npm

        #4. Calcular la UAFzonal=3/Avaluo por Ha
        uaf_zonal = api_sgco_zhg.objects.raw(
            '''select *
                from dane_api_sgco_zhg dasz 
                where zhg_dfcha_rgstro_zhg = (
                    select max(zhg_dfcha_rgstro_zhg)
                    from dane_api_sgco_zhg dasz 
                    where dasz.mpio_id = %s)
                and dasz.mpio_id = %s''', [mpio, mpio])
        
        for item in uaf_zonal:
            try:
                zhg = api_sgco_zhg.objects.get(Q(id=item.id))
                zhg.zhg_nuaf_znal = valor_catastral / zhg.zhg_navluo_hctrea 
                zhg.save()
            except api_sgco_zhg.DoesNotExist:
                print("No se encontró ZHG buscada")   

    def nivel_terreno(self, mpio, predio): 
        """
            - Calcular el # UAF equivalentes de cada predio en cada ZHG=Área de la zona / UAFzonal de dicha zona
            - Totalizar por predio = Sumar agrupando por predio
        """
        area_predio = api_sgco_estr_area_predio.objects.raw(
            '''select 1 as id, SUM(divi) as sum
                from 
                (select daseap.esap_nhctreas/dasz.zhg_nuaf_znal as divi
                from dane_api_sgco_estr dase  
                join dane_api_sgco_estr_area_predio daseap on daseap.estr_id = dase.id
                join dane_api_sgco_zhg dasz on dasz.zhg_icdgo = daseap.esap_izna 
                where dase.mpio_id = %s
                and dase.tiex_id = 11
                and dase.id = %s ) as tabla''', [mpio, predio])

        total = area_predio[0].sum

        if total is None:
            terreno = 0
        elif total >= 0 and total <= 0.45:
            terreno = 1
        elif total >= 0.46 and total <= 1:
            terreno = 2
        elif total >= 1.01 and total <= 2.56:
            terreno = 3
        elif total >= 2.57 and total <= 5.4:
            terreno = 4
        elif total >= 5.41 and total <= 9.65:
            terreno = 5
        else:
            terreno = 6

        return terreno        

    def nivel_vivienda(self, mpio, predio):
        """
            - Mayor = puntaje más alto de todas las viviendas asociadas al predio
        """
        carac_vivienda = api_sgco_estr_caracter_vivienda.objects.raw(
            '''select 1 as id, MAX(dasecv.escv_ipntje) as max
                from dane_api_sgco_estr dase  
                join dane_api_sgco_estr_caracter_vivienda dasecv on dasecv.estr_id = dase.id
                where dase.mpio_id = %s
                and dase.tiex_id = 11
                and dase.id = %s''', [mpio, predio])

        mayor = carac_vivienda[0].max
        
        if mayor is None:
            vivienda = 0
        elif mayor >= 0 and mayor <= 24:
            vivienda = 1
        elif mayor >= 25 and mayor <= 31:
            vivienda = 2
        elif mayor >= 32 and mayor <= 43:
            vivienda = 3
        elif mayor >= 44 and mayor <= 53:
            vivienda = 4
        elif mayor >= 54 and mayor <= 69:
            vivienda = 5
        else:
            vivienda = 6

        return vivienda

    def estratificacion_inicial(self, mpio):
        try:
            ties_vivienda = api_sgco_listas.objects.get(Q(domi=29), Q(lsta_iiditem=1))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de estratificación buscado") 
        
        try:
            ties_terreno = api_sgco_listas.objects.get(Q(domi=29), Q(lsta_iiditem=2))
        except api_sgco_listas.DoesNotExist:
            print("No se encontró el tipo de estratificación buscado")         
        
        # Leer predio 
        predios = api_sgco_estr.objects.filter(Q(mpio=mpio), Q(tiex=11))

        for item in predios:
            # Generar nivel por terreno
            nivel_terreno = self.nivel_terreno(mpio, item.id)
            # Generar nivel por vivienda 
            nivel_vivienda = self.nivel_vivienda(mpio, item.id)

            if nivel_terreno > nivel_vivienda:
                item.estr_iestrto = nivel_terreno
                item.ties = ties_terreno
                item.save()
            else:
                item.estr_iestrto = nivel_vivienda
                item.ties = ties_vivienda
                item.save()