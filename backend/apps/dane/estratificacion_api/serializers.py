from django.db.models import Q
from rest_framework import serializers
from apps.dane.models.estratificacion import *
from apps.dane.models.registro1_igac import *
from apps.dane.models.registro2_igac import *
from apps.dane.models.municipio import *
from apps.dane.models.persona import *
from apps.dane.models.listas import *
from apps.dane.persona_api.serializers import PersonaSerializer
from apps.dane.dominios_api.serializers import ListaSerializer
from apps.dane.municipio_api.serializers import MunicipioSerializer

class ParametrizarEstratificacionSerializer(serializers.Serializer):
    file1 = serializers.FileField(allow_empty_file=False)
    file2 = serializers.FileField(allow_empty_file=False)
    mpio = serializers.PrimaryKeyRelatedField(queryset=api_sgco_municipio.objects.all())

class Registro1IgacSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_sgco_estr_rgstro1_igac
        exclude = ['created', 'modified']

class Registro1IgacArchivoSerializer(serializers.Serializer):
   
    reg1_cnmro_prdial = serializers.CharField(max_length=25)
    reg1_inmro_orden = serializers.IntegerField()
    reg1_ittal_rgstros = serializers.IntegerField()
    reg1_cdrccion = serializers.CharField(max_length=100)
    reg1_icmna = serializers.CharField(max_length=1, allow_null=True)
    deec = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    reg1_narea_trrno = serializers.FloatField()
    reg1_narea_cnstruida = serializers.FloatField()
    reg1_navluo = serializers.FloatField()
    reg1_cvgncia = serializers.CharField(max_length=8)
    reg1_inmro_prdial_ncional = serializers.CharField(max_length=30)
    mpio = serializers.PrimaryKeyRelatedField(queryset=api_sgco_municipio.objects.all())
    treg = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    
    pers_cnmbre = serializers.CharField(max_length=100)
    pers_nidntfccion = serializers.CharField(max_length=25)
    tiid = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    esci = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all(), allow_null=True)

    def create(self, validated_data): 

        if (validated_data.get('tiid') == 'X'):
            try:
                # Si encuentra la busqueda realiza la actualiazación
                persona = api_sgco_persona.objects.get(Q(pers_cnmbre=validated_data.get('pers_cnmbre')),Q(tiid=validated_data.get('tiid')))
                persona.pers_cnmbre = validated_data.get('pers_cnmbre')
                persona.pers_nidntfccion = validated_data.get('pers_nidntfccion')
                persona.tiid = validated_data.get('tiid')
                persona.esci = validated_data.get('esci')
                persona.save()
            except api_sgco_persona.DoesNotExist:
                # Crea el recurso desde cero
                persona = api_sgco_persona.objects.create(
                    pers_cnmbre=validated_data.get('pers_cnmbre'),
                    pers_nidntfccion=validated_data.get('pers_nidntfccion'),
                    tiid=validated_data.get('tiid'),
                    esci=validated_data.get('esci')
                )   
        else:
            try:
                # Si encuentra la busqueda realiza la actualiazación
                persona = api_sgco_persona.objects.get(Q(pers_nidntfccion=validated_data.get('pers_nidntfccion')),Q(tiid=validated_data.get('tiid')))
                persona.pers_cnmbre = validated_data.get('pers_cnmbre')
                persona.pers_nidntfccion = validated_data.get('pers_nidntfccion')
                persona.tiid = validated_data.get('tiid')
                persona.esci = validated_data.get('esci')
                persona.save()
            except api_sgco_persona.DoesNotExist:
                # Crea el recurso desde cero
                persona = api_sgco_persona.objects.create(
                    pers_cnmbre=validated_data.get('pers_cnmbre'),
                    pers_nidntfccion=validated_data.get('pers_nidntfccion'),
                    tiid=validated_data.get('tiid'),
                    esci=validated_data.get('esci')
                )        
        try:
            # Si encuentra la busqueda realiza la actualiazación
            igac1 = api_sgco_estr_rgstro1_igac.objects.get(Q(reg1_cnmro_prdial=validated_data.get('reg1_cnmro_prdial')), Q(mpio=validated_data.get('mpio')),  Q(is_removed=False))
            igac1.reg1_cnmro_prdial=validated_data.get('reg1_cnmro_prdial')
            igac1.reg1_inmro_orden=validated_data.get('reg1_inmro_orden')
            igac1.reg1_ittal_rgstros=validated_data.get('reg1_ittal_rgstros')
            igac1.reg1_cdrccion=validated_data.get('reg1_cdrccion')
            igac1.reg1_icmna=validated_data.get('reg1_icmna')
            igac1.deec=validated_data.get('deec')
            igac1.reg1_narea_trrno=validated_data.get('reg1_narea_trrno')
            igac1.reg1_narea_cnstruida=validated_data.get('reg1_narea_cnstruida')
            igac1.reg1_navluo=validated_data.get('reg1_navluo')
            igac1.reg1_cvgncia=validated_data.get('reg1_cvgncia')
            igac1.reg1_inmro_prdial_ncional=validated_data.get('reg1_inmro_prdial_ncional')
            igac1.mpio=validated_data.get('mpio')
            igac1.pers=persona
            igac1.treg=validated_data.get('treg')
            igac1.save()
        except api_sgco_estr_rgstro1_igac.DoesNotExist:
            # Crea el recurso desde cero
            igac1 = api_sgco_estr_rgstro1_igac.objects.create(
                reg1_cnmro_prdial=validated_data.get('reg1_cnmro_prdial'),
                reg1_inmro_orden=validated_data.get('reg1_inmro_orden'),
                reg1_ittal_rgstros=validated_data.get('reg1_ittal_rgstros'),
                reg1_cdrccion=validated_data.get('reg1_cdrccion'),
                reg1_icmna=validated_data.get('reg1_icmna'),
                deec=validated_data.get('deec'),
                reg1_narea_trrno=validated_data.get('reg1_narea_trrno'),
                reg1_narea_cnstruida=validated_data.get('reg1_narea_cnstruida'),
                reg1_navluo=validated_data.get('reg1_navluo'),
                reg1_cvgncia=validated_data.get('reg1_cvgncia'),
                reg1_inmro_prdial_ncional=validated_data.get('reg1_inmro_prdial_ncional'),
                mpio=validated_data.get('mpio'),
                pers=persona,
                treg=validated_data.get('treg')
            )

        return igac1

class Registro2IgacSerializer(serializers.ModelSerializer):
    class Meta:
        model = api_sgco_estr_rgstro2_igac
        exclude = ['created', 'modified']

class Registro2IgacArchivoSerializer(serializers.Serializer):
    reg2_cnmro_prdial = serializers.CharField(max_length=25)
    reg2_ittal_rgstros = serializers.IntegerField()
    reg2_cvgncia = serializers.CharField(max_length=8)
    inmro_orden = serializers.IntegerField()
    mpio = serializers.PrimaryKeyRelatedField(queryset=api_sgco_municipio.objects.all())
    treg = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    
    cons_ihbtciones1 = serializers.IntegerField()
    cons_ipntje1 = serializers.IntegerField()
    cons_narea_cnstruida1 = serializers.FloatField(allow_null=True)
    uso1 = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    
    cons_ihbtciones2 = serializers.IntegerField()
    cons_ipntje2 = serializers.IntegerField()
    cons_narea_cnstruida2 = serializers.FloatField(allow_null=True)
    uso2 = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    
    cons_ihbtciones3 = serializers.IntegerField()
    cons_ipntje3 = serializers.IntegerField()
    cons_narea_cnstruida3 = serializers.FloatField()
    uso3 = serializers.PrimaryKeyRelatedField(queryset=api_sgco_listas.objects.all())
    
    eszn_narea_trrno1 = serializers.FloatField(allow_null=True)
    eszn_izna_ecnmca1 = serializers.IntegerField()
    eszn_narea_trrno2 = serializers.FloatField(allow_null=True)
    eszn_izna_ecnmca2 = serializers.IntegerField()

    def create(self, validated_data):
        try:
            """
                Si encuentra el recurso en la busqueda realiza la actualización en la tabla IGAC2
                y si lo encuentra borra la información de las tablas relacionadas 
                construcciones y zonas y vuelve a crear la información. Este proceso se debe realizar de esta manera
                ya que las tablas construcciones y zonas no tienen identificadores unicos, que permitan 
                la actualización de un recurso por vez. Por lo tanto se hace de manera masiva por medio del numero 
                de orden.
            """
            igac2 = api_sgco_estr_rgstro2_igac.objects.get(Q(reg2_cnmro_prdial=validated_data.get('reg2_cnmro_prdial')), Q(mpio=validated_data.get('mpio')), Q(is_removed=False))
            igac2.reg2_cnmro_prdial=validated_data.get('reg2_cnmro_prdial')
            igac2.reg2_ittal_rgstros=validated_data.get('reg2_ittal_rgstros')
            igac2.mpio=validated_data.get('mpio')
            igac2.treg=validated_data.get('treg')
            igac2.save()

            construcciones = api_sgco_estr_igac_cnstrcciones.objects.filter(Q(reg2=igac2), Q(cons_inmro_orden=validated_data.get('inmro_orden')))
            construcciones.delete()

            if not validated_data.get('cons_narea_cnstruida1') is None:   
                if (validated_data.get('uso1') != 0 and validated_data.get('cons_narea_cnstruida1') != 0):
                    construcciones_1 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones1'),
                        cons_ipntje=validated_data.get('cons_ipntje1'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida1'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso1')
                    )
            
            if not validated_data.get('cons_narea_cnstruida2') is None:
                if (validated_data.get('uso2') != 0  and validated_data.get('cons_narea_cnstruida2') != 0):
                    construcciones_2 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones2'),
                        cons_ipntje=validated_data.get('cons_ipntje2'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida2'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso2')
                    )

            if not validated_data.get('cons_narea_cnstruida3') is None:
                if (validated_data.get('uso3') != 0 and validated_data.get('cons_narea_cnstruida3') != 0):
                    construcciones_3 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones3'),
                        cons_ipntje=validated_data.get('cons_ipntje3'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida3'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso3')
                    )               
            
            zonas = api_sgco_estr_rgstro2_igac_znas.objects.filter(Q(reg2=igac2), Q(eszn_inmro_orden=validated_data.get('inmro_orden')))
            zonas.delete()

            if not validated_data.get('eszn_narea_trrno1') is None:
                if validated_data.get('eszn_narea_trrno1') != 0:
                    zonas_1 = api_sgco_estr_rgstro2_igac_znas.objects.create(
                        eszn_narea_trrno=validated_data.get('eszn_narea_trrno1'),
                        eszn_izna_ecnmca=validated_data.get('eszn_izna_ecnmca1'),
                        eszn_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2
                    )

            if not validated_data.get('eszn_narea_trrno2') is None:
                if validated_data.get('eszn_narea_trrno2') != 0:
                    zonas_2 = api_sgco_estr_rgstro2_igac_znas.objects.create(
                        eszn_narea_trrno=validated_data.get('eszn_narea_trrno2'),
                        eszn_izna_ecnmca=validated_data.get('eszn_izna_ecnmca2'),
                        eszn_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2
                    )            

        except api_sgco_estr_rgstro2_igac.DoesNotExist:
            """
                Crea el recurso desde cero
            """
            igac2 = api_sgco_estr_rgstro2_igac.objects.create(
                reg2_cnmro_prdial=validated_data.get('reg2_cnmro_prdial'),
                reg2_ittal_rgstros=validated_data.get('reg2_ittal_rgstros'),
                mpio=validated_data.get('mpio'),
                treg=validated_data.get('treg')
            )    

            if not validated_data.get('cons_narea_cnstruida1') is None:
                if (validated_data.get('uso1') != 0 and validated_data.get('cons_narea_cnstruida1') != 0):
                    construcciones_1 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones1'),
                        cons_ipntje=validated_data.get('cons_ipntje1'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida1'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso1')
                    )

            if not validated_data.get('cons_narea_cnstruida2') is None:
                if (validated_data.get('uso2') != 0 and validated_data.get('cons_narea_cnstruida2') != 0):
                    construcciones_2 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones2'),
                        cons_ipntje=validated_data.get('cons_ipntje2'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida2'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso2')
                    )

            if not validated_data.get('cons_narea_cnstruida3') is None:
                if (validated_data.get('uso3') != 0 and validated_data.get('cons_narea_cnstruida3') != 0):
                    construcciones_3 = api_sgco_estr_igac_cnstrcciones.objects.create(
                        cons_ihbtciones=validated_data.get('cons_ihbtciones3'),
                        cons_ipntje=validated_data.get('cons_ipntje3'),
                        cons_narea_cnstruida=validated_data.get('cons_narea_cnstruida3'),
                        cons_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2,
                        uso=validated_data.get('uso3')
                    )

            if not validated_data.get('eszn_narea_trrno1') is None:
                if validated_data.get('eszn_narea_trrno1') != 0:
                    zonas_1 = api_sgco_estr_rgstro2_igac_znas.objects.create(
                        eszn_narea_trrno=validated_data.get('eszn_narea_trrno1'),
                        eszn_izna_ecnmca=validated_data.get('eszn_izna_ecnmca1'),
                        eszn_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2
                    )
            
            if not validated_data.get('eszn_narea_trrno2') is None:
                if validated_data.get('eszn_narea_trrno2') != 0:
                    zonas_2 = api_sgco_estr_rgstro2_igac_znas.objects.create(
                        eszn_narea_trrno=validated_data.get('eszn_narea_trrno2'),
                        eszn_izna_ecnmca=validated_data.get('eszn_izna_ecnmca2'),
                        eszn_inmro_orden=validated_data.get('inmro_orden'),
                        reg2=igac2
                    )

        return igac2

class EstratificacionSerializer(serializers.ModelSerializer):
    movi = ListaSerializer()
    tiex = ListaSerializer()
    time = ListaSerializer()
    mpio = MunicipioSerializer()
    pers = PersonaSerializer()
    esfo = ListaSerializer()

    class Meta:
        model = api_sgco_estr
        exclude = ['created', 'modified']
