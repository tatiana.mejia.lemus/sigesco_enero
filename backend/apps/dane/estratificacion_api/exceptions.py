from rest_framework.exceptions import APIException

class MunicipioNoEncontradoRequest(APIException):
    status_code = 404
    default_detail = "Municipio enviado en el Request no encontrado"
    default_code = "no_encontrado"

class MunicipioNoEncontrado(APIException):
    status_code = 404
    default_detail = "MUNICIPIO no encontrado"
    default_code = "no_encontrado"

class DestinoEconomicoNoEncontrado(APIException):
    status_code = 404
    default_detail = "DESTINO_ECONOMICO no encontrado"
    default_code = "no_encontrado"

class TipoDocumentoNoEncontrado(APIException):
    status_code = 404
    default_detail = "TIPO_DOCUMENTO no encontrado"
    default_code = "no_encontrado"

class EstadoCivilNoEncontrado(APIException):
    status_code = 404
    default_detail = "ESTADO_CIVIL no encontrado"
    default_code = "no_encontrado"

class TipoRegistroNoEncontrado(APIException):
    status_code = 404
    default_detail = "TIPO_DE_REGISTRO no encontrado"
    default_code = "no_encontrado"