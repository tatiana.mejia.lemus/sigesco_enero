from django.urls import path
from .views import *

app_name = 'estratificacion-api'

urlpatterns = [
    # URL para realizar el proceso de parametrización de la estratificación rural
    path('parametrizar', ParametrizarEstratificacionAPIView.as_view()),    
    # URL para consultar reporte de predios excluidos y estratificables
    path('reporte-clasificacion', ReporteClasificacionPrediosAPIView.as_view()), 
    # URL para realizar el proceso de estratificación inicial
    path('estratificacion-inicial', EstratificacionInicialAPIView.as_view()),    
    
]