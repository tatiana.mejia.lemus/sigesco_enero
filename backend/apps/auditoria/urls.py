from django.urls import path, include

app_name = 'auditoria'

urlpatterns = [

    # API de auditoria
    path('auditoria/', include('apps.auditoria.auditoria_api.urls')),

]
