# Generated by Django 3.0.8 on 2020-12-29 16:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dane', '0001_initial'),
        ('auditoria', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalapi_sgco_zhg',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_zhg',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='escv',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_caracter_vivienda', verbose_name='Caracteristicas de la vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='viap_icnsrvcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Estado Conservación'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='viap_ifchda',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Fachada'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='viap_imros',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Muros'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vviend_acbdos_prncples',
            name='viap_ipsos',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Pisos'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='escv',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_caracter_vivienda', verbose_name='Caracteristicas de la vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='vies_iarmzon',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estructura - Armazón'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='vies_icbierta',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estructura - Cubierta'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='vies_icnsrvcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estructura - Estado Conservación'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_estructura',
            name='vies_imros',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estructura - Muros'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='escv',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_caracter_vivienda', verbose_name='Caracteristicas de la vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='vico_icnsrvcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado de la cocina'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='vico_ienchpe',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Cocina - Enchape'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='vico_imbliario',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Cocina - Mobiliario'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_cocina',
            name='vico_itmno',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Cocina - Tamaño'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='escv',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_caracter_vivienda', verbose_name='Caracteristicas de la vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='viba_icnsrvcion',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado del baño'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='viba_ienchpe',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Enchape'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='viba_imbliario',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Acabados - Mobiliario'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_vivienda_banio',
            name='viba_itmno',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Baños - Tamaño'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_user',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_user',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_uaf_documentos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_uaf_documentos',
            name='uaf',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_uaf', verbose_name='Identificador de la Uafpm'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_uaf',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_uaf',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_persona',
            name='esci',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado civil'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_persona',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_persona',
            name='tiid',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de Identificacion'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estrato_atip',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estrato_atip',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estrato_atip',
            name='tiva',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Identificador para el tipo de vivienda atípica'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro2_igac_znas',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro2_igac_znas',
            name='reg2',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_rgstro2_igac', verbose_name='Registro 2'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro2_igac',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro2_igac',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro2_igac',
            name='treg',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de registro catastral'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro1_igac',
            name='deec',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Destino economico'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro1_igac',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro1_igac',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro1_igac',
            name='pers',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_persona', verbose_name='Representante a estratificar'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_rgstro1_igac',
            name='treg',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de registro catastral'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_user',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_user',
            name='req',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_requerimientos', verbose_name='Requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_user',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Usuario DANE al que se asigna el requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_temas',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_temas',
            name='req',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_requerimientos', verbose_name='Requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_temas',
            name='tema',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tema del requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_anexos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos_anexos',
            name='req',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_requerimientos', verbose_name='Requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos',
            name='enti',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_entidad', verbose_name='Entidad'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_requerimientos',
            name='user',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL, verbose_name='Usuario del Dane que asigna el requerimiento'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_predom_zona',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_predom_zona',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_igac_cnstrcciones',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_igac_cnstrcciones',
            name='reg2',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_rgstro2_igac', verbose_name='Registro 2'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_igac_cnstrcciones',
            name='uso',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de uso'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_estado_urbano',
            name='estdo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_estado_urbano',
            name='etpa',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Etapa del proceso Rg en el que se encuentra'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_estado_urbano',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_estado_urbano',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_estado_urbano',
            name='tpo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_entidad',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_entidad',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_caracter_vivienda',
            name='estr',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr', verbose_name='Vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_caracter_vivienda',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_caracter_vivienda',
            name='tpvi',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de Vivienda'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_area_predio',
            name='estr',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr', verbose_name='Predio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr_area_predio',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='esfo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado Formulario'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='movi',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Motivo de la visita'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Departamento y Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='pers',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_persona', verbose_name='Representante a estratificar'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='ties',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de estratificación'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='tiex',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de Exclusion'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_estr',
            name='time',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de metodologia'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_entidad_remitente',
            name='crgo',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Cargo'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_entidad_remitente',
            name='enti',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_estr_entidad', verbose_name='Entidad'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_entidad_remitente',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_archivos_proceso_estr',
            name='espr',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Estado del proceso'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_archivos_proceso_estr',
            name='history_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_archivos_proceso_estr',
            name='mpio',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_municipio', verbose_name='Municipio'),
        ),
        migrations.AddField(
            model_name='historicalapi_sgco_archivos_proceso_estr',
            name='tiar',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='dane.api_sgco_listas', verbose_name='Tipo de archivo'),
        ),
    ]
