from django.http import JsonResponse
from django.http import Http404
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework import status
from sigesco.custom_response import CustomResponse
from apps.auditoria.auditoria_api.serializers import *
from apps.auditoria.pagination import PaginationHandlerMixin
from apps.dane.models.entidad import *
from apps.dane.models.estado_estratificacion import *
from apps.dane.models.estratificacion_archivos import *
from apps.dane.models.estratificacion import *
from apps.dane.models.estrato_predom_zona import *
from apps.dane.models.gestion_requerimientos import *
from apps.dane.models.persona import *
from apps.dane.models.registro1_igac import *
from apps.dane.models.registro2_igac import *
from apps.dane.models.uaf import *
from apps.dane.models.user import *
from apps.dane.models.zhg import *

class AuditoriaAPIView(APIView, PaginationHandlerMixin):
    """
        Funciones para:
        - Consultar los datos de auditoria
    """
    permission_classes = [IsAuthenticated]
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS    

    def get(self, request, format=None, *args, **kwargs):

        modelo = self.request.query_params.get('modelo', None)

        if modelo is not None:
            if modelo == 'api_sgco_estr_entidad':
                model = api_sgco_estr_entidad.history.all()
                serializer_class = api_sgco_estr_entidad_serializer
            elif modelo == 'api_sgco_entidad_remitente':
                model = api_sgco_entidad_remitente.history.all()
                serializer_class = api_sgco_entidad_remitente_serializer
            elif modelo == 'api_sgco_estr_estado_urbano':
                model = api_sgco_estr_estado_urbano.history.all()
                serializer_class = api_sgco_estr_estado_urbano_serializer
            elif modelo == 'api_sgco_archivos_proceso_estr':
                model = api_sgco_archivos_proceso_estr.history.all()
                serializer_class = api_sgco_archivos_proceso_estr_serializer
            elif modelo == 'api_sgco_estr':
                model = api_sgco_estr.history.all()
                serializer_class = api_sgco_estr_serializer
            elif modelo == 'api_sgco_estr_area_predio':
                model = api_sgco_estr_area_predio.history.all()
                serializer_class = api_sgco_estr_area_predio_serializer
            elif modelo == 'api_sgco_estr_caracter_vivienda':
                model = api_sgco_estr_caracter_vivienda.history.all()
                serializer_class = api_sgco_estr_caracter_vivienda_serializer
            elif modelo == 'api_sgco_vivienda_estructura':
                model = api_sgco_vivienda_estructura.history.all()
                serializer_class = api_sgco_vivienda_estructura_serializer
            elif modelo == 'api_sgco_vviend_acbdos_prncples':
                model = api_sgco_vviend_acbdos_prncples.history.all()
                serializer_class = api_sgco_vviend_acbdos_prncples_serializer
            elif modelo == 'api_sgco_vivienda_banio':
                model = api_sgco_vivienda_banio.history.all()
                serializer_class = api_sgco_vivienda_banio_serializer
            elif modelo == 'api_sgco_vivienda_cocina':
                model = api_sgco_vivienda_cocina.history.all()
                serializer_class = api_sgco_vivienda_cocina_serializer
            elif modelo == 'api_sgco_estr_predom_zona':
                model = api_sgco_estr_predom_zona.history.all()
                serializer_class = api_sgco_estr_predom_zona_serializer
            elif modelo == 'api_sgco_estrato_atip':
                model = api_sgco_estrato_atip.history.all()
                serializer_class = api_sgco_estrato_atip_serializer
            elif modelo == 'api_sgco_estr_requerimientos':
                model = api_sgco_estr_requerimientos.history.all()
                serializer_class = api_sgco_estr_requerimientos_serializer
            elif modelo == 'api_sgco_estr_requerimientos_anexos':
                model = api_sgco_estr_requerimientos_anexos.history.all()
                serializer_class = api_sgco_estr_requerimientos_anexos_serializer
            elif modelo == 'api_sgco_estr_requerimientos_temas':
                model = api_sgco_estr_requerimientos_temas.history.all()
                serializer_class = api_sgco_estr_requerimientos_temas_serializer
            elif modelo == 'api_sgco_estr_requerimientos_user':
                model = api_sgco_estr_requerimientos_user.history.all()
                serializer_class = api_sgco_estr_requerimientos_user_serializer
            elif modelo == 'api_sgco_persona':
                model = api_sgco_persona.history.all()
                serializer_class = api_sgco_persona_serializer
            elif modelo == 'api_sgco_estr_rgstro1_igac':
                model = api_sgco_estr_rgstro1_igac.history.all()
                serializer_class = api_sgco_estr_rgstro1_igac_serializer
            elif modelo == 'api_sgco_estr_rgstro2_igac':
                model = api_sgco_estr_rgstro2_igac.history.all()
                serializer_class = api_sgco_estr_rgstro2_igac_serializer
            elif modelo == 'api_sgco_estr_igac_cnstrcciones':
                model = api_sgco_estr_igac_cnstrcciones.history.all()
                serializer_class = api_sgco_estr_igac_cnstrcciones_serializer
            elif modelo == 'api_sgco_estr_rgstro2_igac_znas':
                model = api_sgco_estr_rgstro2_igac_znas.history.all()
                serializer_class = api_sgco_estr_rgstro2_igac_znas_serializer
            elif modelo == 'api_sgco_uaf':
                model = api_sgco_uaf.history.all()
                serializer_class = api_sgco_uaf_serializer
            elif modelo == 'api_sgco_uaf_documentos':
                model = api_sgco_uaf_documentos.history.all()
                serializer_class = api_sgco_uaf_documentos_serializer
            elif modelo == 'api_sgco_user':
                model = api_sgco_user.history.all()
                serializer_class = api_sgco_user_serializer
            elif modelo == 'api_sgco_zhg':
                model = api_sgco_zhg.history.all()
                serializer_class = api_sgco_zhg_serializer
        else:
            return Response({'error': 'El request no contiene ningun modelo'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = self.serializar_modelo(model, serializer_class)        

        return Response(serializer.data, status=status.HTTP_200_OK)

    def serializar_modelo(self, model, serializer_class):        
        page = self.paginate_queryset(model)
        if page is not None:
            serializer = self.get_paginated_response(serializer_class(page, many=True).data)
        else:
            serializer = serializer_class(model, many=True)
        
        return serializer