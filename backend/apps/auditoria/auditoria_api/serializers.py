from django.db.models import Q
from rest_framework import serializers
from apps.dane.models.entidad import *
from apps.dane.models.estado_estratificacion import *
from apps.dane.models.estratificacion_archivos import *
from apps.dane.models.estratificacion import *
from apps.dane.models.estrato_predom_zona import *
from apps.dane.models.gestion_requerimientos import *
from apps.dane.models.persona import *
from apps.dane.models.registro1_igac import *
from apps.dane.models.registro2_igac import *
from apps.dane.models.uaf import *
from apps.dane.models.user import *
from apps.dane.models.zhg import *

class api_sgco_estr_entidad_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_entidad.history.model
        fields = '__all__'

class api_sgco_entidad_remitente_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_entidad_remitente.history.model
        fields = '__all__'

class api_sgco_estr_estado_urbano_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_estado_urbano.history.model
        fields = '__all__'

class api_sgco_archivos_proceso_estr_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_archivos_proceso_estr.history.model
        fields = '__all__'

class api_sgco_estr_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr.history.model
        fields = '__all__'

class api_sgco_estr_area_predio_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_area_predio.history.model
        fields = '__all__'

class api_sgco_estr_caracter_vivienda_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_caracter_vivienda.history.model
        fields = '__all__'

class api_sgco_vivienda_estructura_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_vivienda_estructura.history.model
        fields = '__all__'

class api_sgco_vviend_acbdos_prncples_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_vviend_acbdos_prncples.history.model
        fields = '__all__'

class api_sgco_vivienda_banio_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_vivienda_banio.history.model
        fields = '__all__'

class api_sgco_vivienda_cocina_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_vivienda_cocina.history.model
        fields = '__all__'

class api_sgco_estr_predom_zona_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_predom_zona.history.model
        fields = '__all__'

class api_sgco_estrato_atip_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estrato_atip.history.model
        fields = '__all__'

class api_sgco_estr_requerimientos_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_requerimientos.history.model
        fields = '__all__'
        
class api_sgco_estr_requerimientos_anexos_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_requerimientos_anexos.history.model
        fields = '__all__'

class api_sgco_estr_requerimientos_temas_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_requerimientos_temas.history.model
        fields = '__all__'

class api_sgco_estr_requerimientos_user_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_requerimientos_user.history.model
        fields = '__all__'

class api_sgco_persona_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_persona.history.model
        fields = '__all__'

class api_sgco_estr_rgstro1_igac_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_rgstro1_igac.history.model
        fields = '__all__'

class api_sgco_estr_rgstro2_igac_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_rgstro2_igac.history.model
        fields = '__all__'

class api_sgco_estr_igac_cnstrcciones_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_igac_cnstrcciones.history.model
        fields = '__all__'

class api_sgco_estr_rgstro2_igac_znas_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_estr_rgstro2_igac_znas.history.model
        fields = '__all__'

class api_sgco_uaf_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_uaf.history.model
        fields = '__all__'

class api_sgco_uaf_documentos_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_uaf_documentos.history.model
        fields = '__all__'

class api_sgco_user_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_user.history.model
        fields = '__all__'

class api_sgco_zhg_serializer(serializers.ModelSerializer):

    class Meta:
        model = api_sgco_zhg.history.model
        fields = '__all__'
