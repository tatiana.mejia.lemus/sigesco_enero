from django.urls import path
from .views import *

app_name = 'auditoria-api'

urlpatterns = [
    # URL que lista todos los campos de las tablas de auditoria
    path('auditoria', AuditoriaAPIView.as_view()),

]