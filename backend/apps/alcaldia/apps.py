from django.apps import AppConfig


class AlcaldiaConfig(AppConfig):
    name = 'alcaldia'
