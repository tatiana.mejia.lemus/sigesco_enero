# Sistema de Gestión de la Estratificación y las Coberturas de Servicios Públicos Domiciliarios - SIGESCO

## Documentación del API

https://documenter.getpostman.com/view/1390520/TVRecpuC

## Ayuda 

Comando para ingresar al bash del contenedor de backend, alli se pueden ejecutar, instrucciones directamente al servidor

```
docker-compose run backend bash
```

Comando para crear un super usuario en Django 

```
python manage.py createsuperuser
```
