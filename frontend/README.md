# Sistema de Gestión de la Estratificación y las Coberturas de Servicios Públicos Domiciliarios - SIGESCO

## Frontend 

🚀

### Proceso de autenticación 

La tecnologiía usada para el proceso de autenticación es JWT. Para realizar el proceso respectivo se debe consultar la siguiente URL 

```
http://localhost:8000/dane/api/token/
```
Al enviar el siguiente JSON a la URL:

```
{
    "username": "",
    "password": "",
}
```
La URL debe responder:

```
{
    "refresh": "",
    "access": "TOKEN",
}
```
Para realizar consultas a cualquier URL del API se debe enviar el TOKEN generado, dentro del header:

```
Authorization: Bearer TOKEN
```
### Ayuda 
