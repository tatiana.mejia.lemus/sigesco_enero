# Sistema de Gestión de la Estratificación y las Coberturas de Servicios Públicos Domiciliarios - SIGESCO

SIGESCO es una iniciativa de desarrollo tecnológico liderada por el DANE para dar una solución innovadora a los problemas actuales en la gestión de la información de estratificación y a las coberturas de los servicios públicos domiciliarios en Colombia

## Instalación del ambiente Docker

Construye, (re) crea, inicia y conecta contenedores para un servicio.

```
docker-compose up
```
Detiene contenedores y elimina contenedores, redes, volúmenes e imágenes creadas por up
```
docker-compose down
```

## Conexión a la base de datos Postgres desde el host local 

La conexión se puede realizar desde cualquier IDE de base de datos que tenga instalado el host local, esto se hace mediante los siguientes parámetros:

```
Host: localhost
Port: 6543
Database: SIGESCO
Username: dane
Password: S1g3sc0_D4n3
```

## Usuario no administrador

```
DJANGO_SU_NAME=sigesco
DJANGO_SU_EMAIL=sigesco@sigesco.com
DJANGO_SU_PASSWORD=123456789
```


## Super Usuario

```
DJANGO_SU_NAME=administrador
DJANGO_SU_EMAIL=administrador@sigesco.com
DJANGO_SU_PASSWORD=hola$$12
```

## URLs de SIGESCO

Urls para la generación de TOKEN / Autenticación 

```
api/v1/token

Body:
{
    "username": "",
    "password": ""
}

api/v1/token/refresh

Body: 
{
    "username": "",
    "password": "",
    "refresh": ""
}
```
Estructura básica de una URL

_Nota: El nombre del API es en plural, por ejemplo: personas_

```
api/v1/<NOMBRE_DEL_API>/<NOMBRE_DEL_MODELO>
api/v1/<NOMBRE_DEL_API>/<NOMBRE_DEL_MODELO>/<INT:PK>

Headers:
- Authorization: JWT + Token
```

## Documentación del API

https://documenter.getpostman.com/view/1390520/TVRecpuC

